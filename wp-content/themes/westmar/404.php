<?php
/**
 *	404 page template
 *
 *	Generates the output for the
 *	404 page.
 *
 *  @package WordPress
 *  @since 1.0
 */

get_header(); ?>

<div class="page-wrap">

	<?php //get_template_part( 'partials/components/page-header', 'page' ); ?>

	<div class="page-content container">
		<div class="icon"><img src="<?php echo THEME_URL; ?>/assets/img/icon-404.svg" alt="404 Image"></div>
		<h1 class="title">Well this is awkward...</h1>
		<p>The page you're searching for does not exist.</p>
		<a href="#" class="btn" onclick="history.back(-1)">Go Back</a>
	</div>

</div> <!-- END .page-wrap -->

<?php get_footer(); ?>
