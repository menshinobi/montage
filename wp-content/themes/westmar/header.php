<!DOCTYPE html>
<!--[if IE]><html class="no-js is-ie"><![endif]-->
<!--[if !IE]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo THEME_URL; ?>/assets/img/icons/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo THEME_URL; ?>/assets/img/icons/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo THEME_URL; ?>/assets/img/icons/favicon-16x16.png" sizes="16x16" />
	<meta name="application-name" content="SITE NAME"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo THEME_URL; ?>/assets/img/icons/mstile-144x144.png" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php get_template_part( 'partials/header/header-menu' ); ?>
