<?php
/**
 *	Single post template
 *
 *	Populates the content for a single post
 *
 *	@package WordPress
 *	@since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<div class="page-wrap"">

		<?php
		while ( have_posts() ) : the_post();
			$featured_image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
			! empty( $featured_image_url ) ? $header_style = 'style="background-image: url('. $featured_image_url.');"'  : $header_style = 'style="background-image: url('. THEME_URL . '/assets/img/post-header.jpg);"';


			$categories = get_the_category();
			if ( ! empty( $categories ) ) {
			    $the_post_category = $categories[0]->name;
			}

		?>

		<div class="page-header" <?php echo $header_style; ?>>

			<div class="page-header__overlay"></div><!-- .page_header__overlay -->

			<div class="page-header__content">
				<h1 class="page-header__title"><?php the_title(); ?></h1>
				<span class="page-header__subtitle">
					<br />
					<span class="date"><?php echo get_the_date(); ?></span>
				</span>
			</div>
			<!-- .page-header__content -->

		</div>
		<!-- END .page-header -->

		<div class="post-content-wrap container-full">

			<div class="container">

				<?php
					/**
					 * Post Meta Information
					 * Author, Date, Categories, Sharing, Comments.
					 */
					// get_template_part('partials/posts/posts-meta');
				?>

				<article class="post-content">
					<h2 class="post-content__title"><?php the_title(); ?> <span class="category"><span class="inner"><?php echo $the_post_category; ?></span></span><span class="line"></span></h2>
					<?php the_content(); ?>
				</article><!-- .post-content -->

				<div class="sidebar sidebar--post">
					<?php dynamic_sidebar( 'blog-sidebar' ); ?>
				</div><!-- .sidebar -->

				<div class="post-comments-container" id="post-comments">
					<?php comments_template(); ?>
				</div>

			</div>

		</div>
		<!-- END .post-content-wrap -->

	<?php endwhile; ?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
