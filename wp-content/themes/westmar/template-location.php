<?php
/**
 * Template Name: Location
 *
 * This page contains necessary markup for the location page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<div class="page-wrap">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php

			/**
			 * Get the affiliates section.
			 */
			get_template_part( 'partials/components/affiliates' );

			/**
			 * Get the map section.
			 */
			get_template_part( 'partials/components/location', 'map' );

			/**
			 * Get the shuttle section.
			 */
			get_template_part( 'partials/components/location', 'shuttle' );

			/**
			 * Get the shuttle section.
			 */
			get_template_part( 'partials/components/feature-highlight' );

			/**
			 * Get page footer link section.
			 */
			get_template_part( 'partials/components/page', 'footer-link' );

			?>

		<?php endwhile; ?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
