<?php
/**
 *	Default page template
 *
 *	Generates the output for the
 *	pages using the default tempalte
 *
 *  @package WordPress
 *  @since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<div class="page-wrap">

		<?php while ( have_posts() ) : the_post(); ?>

		<?php
		/**
		 * Get the page header component.
		 */
		//get_template_part( 'partials/components/page-header', 'page' );
		?>

		<div class="page-content">

			<div class="container">
				<?php the_content(); ?>
			</div>

		</div><!-- .page-content -->

	<?php endwhile; ?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
