<?php
/**
 * Template Name: Amenities (Perks)
 *
 * This page contains necessary markup for the amenities page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<div class="page-wrap">

		<?php

		while ( have_posts() ) : the_post();

			/**
			 * Get the Photo Amenities section.
			 */
			get_template_part( 'partials/components/amenities', 'photo-list' );

			/**
			 * Get the Amenities chart.
			 */
			get_template_part( 'partials/components/amenities', 'chart' );

			/**
			 * Get the Community Amenities section.
			 */
			get_template_part( 'partials/components/amenities', 'community-list' );

			/**
			 * Get the Unit Amenities section.
			 */
			get_template_part( 'partials/components/amenities', 'apartment-list' );

			/**
			 * Get page footer link section.
			 */
			get_template_part( 'partials/components/page', 'footer-link' );

		endwhile;

		?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
