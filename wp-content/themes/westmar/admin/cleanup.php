<?php
/**
 *
 *	This file removes any unnecessary functionality
 *	that WordPress provides by default, in an effort
 *	to improve speed and experience.
 *
 */

/******************************************
*
*	REMOVE WORDPRESS VERSIONS FROM FILE NAMES
*
******************************************/
function wm_remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'wm_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'wm_remove_wp_version_strings' );

/******************************************
*
*	REMOVE WORDPRESS VERSIONS FROM GENERATOR META
*
******************************************/
function wm_remove_version() {
return '';
}
add_filter('the_generator', 'wm_remove_version');
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );

/******************************************
*
*	REMOVE EMOJI SCRIPTS
*
******************************************/
function wm_disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'wm_disable_emojicons_tinymce' );
}
add_action( 'init', 'wm_disable_wp_emojicons' );

function wm_disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

/******************************************
*
*	SET DEFAULT EDITOR FOR PAGES/POSTS
*
******************************************/
function wm_default_editor() {
    $editor = 'html'; // html or tinymce
    return $editor;
}
add_filter( 'wp_default_editor', 'wm_default_editor' );