<?php
/******************************************
*
*	REGISTER THEME MENU AREAS
*
******************************************/
register_nav_menus(array(
    'wm-nav-main' 	=> 'Main Navigation',
    'wm-nav-top' 	=> 'Top Navigation',
    'wm-nav-footer' => 'Footer Navigation',
));

/******************************************
*
*	CONSTRUCT THEME MENUS
*
******************************************/
if ( ! function_exists( 'wm_main_menu' ) ) {
	function wm_main_menu() {
	    wp_nav_menu(array(
	        'container' => false,                           // remove nav container
	        'container_class' => '',                        // class of container
	        'menu' => '',                                   // menu name
	        'menu_class' => 'main-nav',           			// adding custom nav class
	        'theme_location' => 'wm-nav-main',             // where it's located in the theme
	        'before' => '',                                 // before each link <a>
	        'after' => '',                                  // after each link </a>
	        'link_before' => '',                            // before each link text
	        'link_after' => '',                             // after each link text
	        'depth' => 5,                                   // limit the depth of the nav
	        'fallback_cb' => false,                         // fallback function (see below)
	    ));
	}
}

if ( ! function_exists( 'wm_top_menu' ) ) {
	function wm_top_menu() {
	    wp_nav_menu(array(
	        'container' => false,                           // remove nav container
	        'container_class' => '',                        // class of container
	        'menu' => '',                                   // menu name
	        'menu_class' => 'top-nav',           			// adding custom nav class
	        'theme_location' => 'wm-nav-top',             // where it's located in the theme
	        'before' => '',                                 // before each link <a>
	        'after' => '',                                  // after each link </a>
	        'link_before' => '',                            // before each link text
	        'link_after' => '',                             // after each link text
	        'depth' => 1,                                   // limit the depth of the nav
	        'fallback_cb' => false,                         // fallback function (see below)
	    ));
	}
}

if ( ! function_exists( 'wm_footer_menu' ) ) {
	function wm_footer_menu() {
	    wp_nav_menu(array(
	        'container' => false,                           // remove nav container
	        'container_class' => '',                        // class of container
	        'menu' => '',                                   // menu name
	        'menu_class' => 'footer-nav',           			// adding custom nav class
	        'theme_location' => 'wm-nav-footer',             // where it's located in the theme
	        'before' => '',                                 // before each link <a>
	        'after' => '',                                  // after each link </a>
	        'link_before' => '',                            // before each link text
	        'link_after' => '',                             // after each link text
	        'depth' => 1,                                   // limit the depth of the nav
	        'fallback_cb' => false,                         // fallback function (see below)
	    ));
	}
}

/**
 *  Phone Menu Item.
 *
 * This function adds a menu option to the right hand menu
 * that contains the search icon.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_menu_phone( $items, $args ) {

	$phone_number 	= get_option( 'options_phone_number' );
	$link 			= str_replace( array( '(', ')', ' ', '-' ), '', $phone_number);

	if ( $args->theme_location == 'wm-nav-main' ) :

		$items .= '<div class="menu-item menu-item--divider"></div>';

		$items .= '<li class="menu-item menu-item--mobile">';
		$items .= '<a target="_blank" href="https://westmarstudentlofts.prospectportal.com/Apartments/module/application_authentication/property%5Bid%5D/120554/show_in_popup/false/kill_session/1/">Apply</a>';
		$items .= '</li>';

		$items .= '<li class="menu-item menu-item--mobile">';
		$items .= '<a target="_blank" href="https://westmarlofts.securecafe.com/residentservices/westmar-student-lofts/userlogin.aspx">Residents</a>';
		$items .= '</li>';

		$items .= '<li class="menu-item menu-item--phone menu-item--mobile">';
		$items .= '<a href="tel' . $link . '">' . $phone_number . '</a>';
		$items .= '</li>';

	endif;

   return $items;
}
add_filter( 'wp_nav_menu_items', 'wm_menu_phone', 10, 2 );
