<?php

$gallery = new CPT(
	array(
		'post_type_name' => 'gallery',
		'singular' => 'Gallery ',
		'plural' => 'Galleries',
		'slug' => 'gallery',
		'menu'	=> 'Gallery',
	),
	array(
		'supports' => array( 'title' ),
		'has_archive'	=> false,
	)
);

$gallery->register_taxonomy(array(
	'taxonomy_name' => 'gallery_category',
	'singular' 		=> 'Gallery Category',
	'plural' 		=> 'Gallery Categories',
	'slug' 			=> 'gallery-category',
));

$gallery->columns(array(
	'cb' 					=> '<input type="checkbox" />',
    'title' 				=> __('Title'),
    'gallery_category' 		=> __('Category'),
    'date' 					=> __('Date')
));

$gallery->menu_icon( 'dashicons-format-gallery' );
