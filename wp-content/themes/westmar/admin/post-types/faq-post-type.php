<?php

$faqs = new CPT(
	array(
		'post_type_name' => 'faqs',
		'singular' => 'FAQ ',
		'plural' => 'FAQs',
		'slug' => 'faq',
		'menu'	=> 'FAQs',
	),
	array(
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'	=> false,
		// 'menu_position' => 2,
	)
);

$faqs->columns(array(
	'cb' 		=> '<input type="checkbox" />',
    'title' 	=> __('Title'),
    'date' 		=> __('Date')
));

$faqs->menu_icon( 'dashicons-format-status' );
