<?php
require_once( THEME_DIR . '/admin/post-types/class-post-type.php' );

// Add additional post types.
require_once( THEME_DIR . '/admin/post-types/faq-post-type.php' );
require_once( THEME_DIR . '/admin/post-types/floor-plan-post-type.php' );
require_once( THEME_DIR . '/admin/post-types/gallery-post-type.php' );
