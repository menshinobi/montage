<?php

$floor_plans = new CPT(
	array(
		'post_type_name' 	=> 'floor-plan',
		'singular' 			=> 'Floor Plan',
		'plural' 			=> 'Floor Plans',
		'slug' 				=> 'floor-plan',
		'menu'				=> 'Floor Plans',
	),
	array(
		'supports' => array( 'title', 'thumbnail', 'editor' ),
		'has_archive'	=> false,
	)
);

$floor_plans->register_taxonomy(array(
	'taxonomy_name' => 'floor-plan-category',
	'singular' 		=> 'Floor Plan Category',
	'plural' 		=> 'Floor Plan Categories',
	'slug' 			=> 'floor-plan-category',
));

$floor_plans->columns(array(
	'cb' 						=> '<input type="checkbox" />',
    'title' 					=> __('Title'),
    'floor-plan-category' 		=> __('Category'),
    'date' 						=> __('Date')
));

$floor_plans->menu_icon( 'dashicons-admin-home' );
