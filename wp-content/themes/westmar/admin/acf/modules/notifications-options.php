<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_578721c798e83',
	'title' => 'Notification Options',
	'fields' => array (
		array (
			'key' => 'field_5787238362064',
			'label' => 'Background Color',
			'name' => 'background_color',
			'type' => 'color_picker',
			'instructions' => 'Select the background color of the notification bar.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 33,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
		array (
			'key' => 'field_578723c562065',
			'label' => 'Text Color',
			'name' => 'text_color',
			'type' => 'color_picker',
			'instructions' => 'Select the color for the text inside of the notification bar.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 34,
				'class' => '',
				'id' => '',
			),
			'default_value' => '#FFFFFF',
		),
		array (
			'key' => 'field_578721d441259',
			'label' => 'Enable Button',
			'name' => 'enable_button',
			'type' => 'radio',
			'instructions' => 'Would you like to display a button?',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 33,
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'yes' => 'Yes',
				'no' => 'No',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_578722274125a',
			'label' => 'Button Text',
			'name' => 'button_text',
			'type' => 'text',
			'instructions' => 'Enter the text that will be displayed inside of the button.',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_578721d441259',
						'operator' => '==',
						'value' => 'yes',
					),
				),
			),
			'wrapper' => array (
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Learn More',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_578722504125b',
			'label' => 'Button Link',
			'name' => 'button_link',
			'type' => 'url',
			'instructions' => 'Enter the URL where the button will lead to when it is clicked.',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_578721d441259',
						'operator' => '==',
						'value' => 'yes',
					),
				),
			),
			'wrapper' => array (
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'http://example.com',
		),
		array (
			'key' => 'field_5787687c11990',
			'label' => 'Include or Exclude Pages',
			'name' => 'include_exclude_pages',
			'type' => 'radio',
			'instructions' => 'If you\'d like to include or exclude the notification on specific pages, you can select them here.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 33,
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'all' => 'Show On All Pages',
				'include' => 'Include On Specific Pages',
				'exclude' => 'Exclude On Specific Pages',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => 'all',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_578740ec71908',
			'label' => 'Pages',
			'name' => 'pages',
			'type' => 'post_object',
			'instructions' => 'Select Pages',
			'required' => 0,
			// 'conditional_logic' => array (
			// 	array (
			// 		array (
			// 			'field' => 'field_5787687c11990',
			// 			'operator' => '!=',
			// 			'value' => 'all',
			// 		),
			// 	),
			// ),
			'wrapper' => array (
				'width' => 34,
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'page',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'id',
			'ui' => 1,
		),
		array (
			'key' => 'field_5787687c119940',
			'label' => 'Display interval',
			'name' => 'display_interval',
			'type' => 'radio',
			'instructions' => 'Select the interval that the notification bar should re-appear after the user clicks the "X".',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 33,
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'all' => 'Reset every page load',
				'1' => 'Reset every day',
				'7' => 'Reset every week',
				'30' => 'Reset every month',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => 'all',
			'layout' => 'horizontal',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'notifications',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
