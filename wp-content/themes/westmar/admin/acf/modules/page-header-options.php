<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_57888a9fcffd4',
	'title' => 'Page Header Options',
	'fields' => array (
		array (
			'key' => 'field_57888ac79851f',
			'label' => 'Header Title',
			'name' => 'header_title',
			'type' => 'text',
			'instructions' => 'Enter a page header title (defaults to the page title)',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50%',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'Title..',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_57888b0a98520',
			'label' => 'Header Subtitle',
			'name' => 'header_subtitle',
			'type' => 'text',
			'instructions' => 'Enter a page header subtitle.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50%',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'Subtitle...',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
			array (
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'template-home.php',
			),
			array (
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'template-contact.php',
			),
			array (
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'template-gallery.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
