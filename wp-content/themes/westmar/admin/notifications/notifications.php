<?php
/**
 * Include Notification Files
 *
 * This file will include the necessary notification files.
 *
 * @package WordPress
 * @since 1.0
 */

define( 'NOTIFICATIONS_ASSETS_URL', THEME_URL . '/admin/notifications/assets' );

if ( is_admin() ) :

	require_once dirname( __FILE__ ) . '/includes/metabox.php';

endif;
