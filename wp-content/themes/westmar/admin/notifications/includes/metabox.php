<?php
/**
 * Add Metaboxes for expiry and active
 *
 * This file contians the functionality that will add metaboxes
 * for notification expiry and active/deactive.
 *
 * @package WordPress
 * @since 1.0
 */
function notification_add_expiration_field() {
	global $post;

	$post_type = get_post_type();
	if ( 'notifications' === $post_type ) :

		if ( ! empty( $post->ID ) ) :

			$expires = get_post_meta( $post->ID, 'notification_expiration', true );
			$show_the_news = get_post_meta( $post->ID, 'notification_active', true );

		endif;

		$label = ! empty( $expires ) ? date_i18n( 'Y-n-d', strtotime( $expires ) ) : __( 'never', 'pw-spe' );
		$date  = ! empty( $expires ) ? date_i18n( 'Y-n-d', strtotime( $expires ) ) : '';
?>

	<div id="pw-spe-expiration-wrap" class="misc-pub-section">
		<span>
			<span class="wp-media-buttons-icon dashicons dashicons-hidden"></span>&nbsp;
			<?php _e( 'Expires:', 'pw-spe' ); ?>
			<b id="pw-spe-expiration-label"><?php echo $label; ?></b>
		</span>
		<a href="#" id="pw-spe-edit-expiration" class="pw-spe-edit-expiration hide-if-no-js">
			<span aria-hidden="true"><?php _e( 'Edit', 'pw-spe' ); ?></span>&nbsp;
			<span class="screen-reader-text"><?php _e( 'Edit date and time', 'pw-spe' ); ?></span>
		</a>
		<div id="pw-spe-expiration-field" class="hide-if-js">
			<p>
				<input type="text" name="pw-spe-expiration" id="pw-spe-expiration" value="<?php echo esc_attr( $date ); ?>" placeholder="yyyy-mm-dd"/>
			</p>
			<p>
				<a href="#" class="pw-spe-hide-expiration button secondary"><?php _e( 'OK', 'pw-spe' ); ?></a>
				<a href="#" class="pw-spe-hide-expiration cancel"><?php _e( 'Cancel', 'pw-spe' ); ?></a>
			</p>
		</div>
		<?php wp_nonce_field( 'notification_edit_expiration', 'notification_expiration_nonce' ); ?>
	</div>

	<div id="show-news" class="misc-pub-section">
		<span>
			<span class="wp-media-buttons-icon dashicons dashicons-megaphone"></span>&nbsp;
			<?php _e( 'Make Active:', 'pw-spe' ); ?>

			<?php if ( ! empty( $show_the_news ) && 'yes' === $show_the_news ) : ?>
				<input type="radio" name="show-news" value="yes" checked/><label for="show-news">Yes</label>
			<?php else :  ?>
				<input type="radio" name="show-news" value="yes"/><label for="show-news">Yes</label>
			<?php endif; ?>

			<?php if ( ! empty( $show_the_news ) && 'no' === $show_the_news ) : ?>
				<input type="radio" name="show-news" value="no" checked/><label for="show-news">No</label>
			<?php else : ?>
				<input type="radio" name="show-news" value="no"/><label for="show-news">No</label>
			<?php endif; ?>
		</span>
		<?php wp_nonce_field( 'notification_edit_active', 'notification_active_nonce' ); ?>
	</div>

<?php
endif;

}
add_action( 'post_submitbox_misc_actions', 'notification_add_expiration_field' );

function notification_save_expiration( $post_id = 0 ) {

	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX') && DOING_AJAX ) || isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	if ( ! empty( $_POST['notification_edit_expiration'] ) ) {
		return;
	}

	if ( ! empty( $_POST['notification_edit_active'] ) ) {
		return;
	}

	if ( ! wp_verify_nonce( $_POST['notification_expiration_nonce'], 'notification_edit_expiration' ) ) {
		return;
	}

	if ( ! wp_verify_nonce( $_POST['notification_active_nonce'], 'notification_edit_active' ) ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	$expiration = ! empty( $_POST['pw-spe-expiration'] ) ? sanitize_text_field( $_POST['pw-spe-expiration'] ) : false;
	$show_news = ! empty( $_POST['show-news'] ) ? sanitize_text_field( $_POST['show-news'] ) : false;

	if( $expiration ) {

		/**
		  * Save the expiration
		  */
		update_post_meta( $post_id, 'notification_expiration', $expiration );
		update_post_meta( $post_id, 'notification_active', $show_news );

	} else {

		/**
		  * Remove any existing expiration date
		  */
		delete_post_meta( $post_id, 'notification_expiration' );
		delete_post_meta( $post_id, 'notification_active' );

	}

}
add_action( 'save_post', 'notification_save_expiration' );

function notification_expire_news_scripts() {

	wp_enqueue_style( 'jquery-ui-css', NOTIFICATIONS_ASSETS_URL . '/css/jquery-ui-fresh.min.css' );
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_script( 'jquery-ui-slider' );
	wp_enqueue_script( 'pw-spe-expiration', NOTIFICATIONS_ASSETS_URL . '/js/edit.js' );

}

add_action( 'load-post-new.php', 'notification_expire_news_scripts' );
add_action( 'load-post.php', 'notification_expire_news_scripts' );
