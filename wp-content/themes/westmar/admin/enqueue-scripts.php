<?php
/**
 * Enqueue Scripts
 *
 * This file will add the necessary theme styles and scripts
 *
 * @package WordPress
 * @since 1.0
 */

/**
 * THEME SCRIPTS
 */
function wm_add_scripts() {

    /**
     * Main CSS files
     */
    wp_register_style( 'app-styles', THEME_URL . '/assets/css/app.css' );
    wp_enqueue_style( 'app-styles' );

    /**
     * To customize the styles of this theme without using sass,
     * simply comment out "wp_enqueue_style( 'app-styles' );" above
     * and uncomment the two lines below. This will enable the default
     * "style.css" file to be enqueued without loosing existing styles.
     */
    // wp_register_style( 'main-styles', THEME_URL . '/style.css' );
    // wp_enqueue_style( 'main-styles' );

    $query_args = array(
        'family' => 'Poppins:300,400,500,600,700|Roboto+Condensed',
        'subset' => 'latin,latin-ext',
    );
    wp_enqueue_style( 'google_fonts', add_query_arg( $query_args, '//fonts.googleapis.com/css' ), array(), null );

    /**
     * Main Javascript files
     */
    wp_register_script( 'app-js', THEME_URL . '/assets/js/min/app.min.js', array(), '', true );
    wp_localize_script( 'app-js', 'wmJS', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'app-js' );

    /**
     * Contact Page Scripts
     */
    // if ( is_page_template( 'template-contact.php' ) ) :
    //     wp_enqueue_script( 'goolge-map', 'https://maps.googleapis.com/maps/api/js', array(), '', false );
    //     wp_enqueue_script( 'contact-map', THEME_URL . '/assets/js/contact-map.js', array(), '', true );
    // endif;

}
add_action( 'wp_enqueue_scripts', 'wm_add_scripts' );

/**
 * Google Maps API Key
 *
 * Add the google maps API key to the backend scripts that require it.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_google_maps_api_key() {
    acf_update_setting('google_api_key', 'xxx');
}
add_action('acf/init', 'wm_google_maps_api_key');

/**
 * Add custom user scrpits to footer.
 *
 * Adds the scripts input by the user to the footer.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_add_footer_scripts() {

    $wm_js = get_option( 'options_custom_code' );
    $script_type = 'footer_js';

    if ( ! empty( $wm_js ) ) :

        $i = 0;
        foreach ( $wm_js as $script ) :

            if ( $script_type === $script ) :
                $script_code = get_option( 'options_custom_code_'.$i.'_footer_js_module' );
                echo $script_code;
            endif;

        $i++;
        endforeach;

    endif;

}
add_action( 'wp_footer', 'wm_add_footer_scripts', 21 );



/**
 * Add custom user scrpits to header.
 *
 * Adds the scripts input by the user to the header.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_add_header_scripts() {

    $wm_js = get_option( 'options_custom_code' );
    $script_type = 'header_js';

    if ( ! empty( $wm_js ) ) :

        $i = 0;
        foreach ( $wm_js as $script ) :

            if ( $script_type === $script ) :
                $script_code = get_option( 'options_custom_code_'.$i.'_header_js_module' );
                echo $script_code;
            endif;

        $i++;
        endforeach;

    endif;

}
add_action( 'wp_head', 'wm_add_header_scripts' );

/**
 * Add custom user CSS to header.
 *
 * Adds the CSS input by the user to the header.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_add_header_css() {

    $wm_js = get_option( 'options_custom_code' );
    $script_type = 'css';

    if ( ! empty( $wm_js ) ) :

        $i = 0;
        foreach ( $wm_js as $script ) :

            if ( $script_type === $script ) :
                $script_code = get_option( 'options_custom_code_'.$i.'_css_module' );
                echo $script_code;
            endif;

        $i++;
        endforeach;

    endif;

}
add_action( 'wp_head', 'wm_add_header_css', 9 );

