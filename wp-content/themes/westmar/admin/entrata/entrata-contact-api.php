<?php

function entrata_contact_api( $contact_form ) {

    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) :

        $data = $submission->get_posted_data();

        if ( 45 == $data['_wpcf7'] ) :

            $first_name     = ( isset( $data['sender-first-name'] ) ) ? $data['sender-first-name'] : null;
            $last_name      = ( isset( $data['sender-last-name'] ) ) ? $data['sender-last-name'] : null;
            $email          = ( isset( $data['sender-email'] ) ) ? $data['sender-email'] : null;
            $phone          = ( isset( $data['sender-phone'] ) ) ? $data['sender-phone'] : null;
            $message        = ( isset( $data['sender-message'] ) ) ? $data['sender-message'] : null;

            $jsonRequest = '
            {
                "auth": {
                    "type": "basic",
                    "username": "sam.winn",
                    "password": "Gnomes11"
                },
                "requestId": "15",
                "method": {
                    "name": "sendMitsLeads",
                    "params": {
                        "propertyId": "120554",
                        "Prospects": {
                            "Prospect": [
                            {
                                "TransactionData": {
                                    "OriginatingLeadSource": "Community Website",
                                    "InternetListingService": "ILS Name"
                                },
                                "LastUpdateDate": "'.date('Y-m-d', strtotime('-1 day')).'",
                                "LeasingAgentId": 310485,
                                "Customers": {
                                    "Customer": {
                                        "Name": {
                                            "FirstName": "'.$first_name.'",
                                            "LastName": "'.$last_name.'"
                                        },
                                        "Phone": {
                                            "@attributes": {
                                                "PhoneType": "personal"
                                            },
                                            "PhoneNumber": "'.$phone.'"
                                        },
                                        "Email": "'.$email.'"
                                    }
                                },
                                "CustomerPreferences": {
                                    "Comment": "'.$message.'"
                                }
                            }]
                        }
                    }
                }
            }';

            /* Initiate a CURL resource */
            $resCurl = curl_init();
            /* If you want to send a JSON Request, use these options */
            curl_setopt( $resCurl, CURLOPT_HTTPHEADER,  array( 'Content-type: APPLICATION/JSON; CHARSET=UTF-8' ) );
            curl_setopt( $resCurl, CURLOPT_POSTFIELDS, $jsonRequest );
            curl_setopt( $resCurl, CURLOPT_POST, true );
            curl_setopt( $resCurl, CURLOPT_URL, 'https://cardinal.entrata.com/api/leads' );
            curl_setopt( $resCurl, CURLOPT_RETURNTRANSFER, 1);

            if( curl_exec( $resCurl ) === false ) {
                //echo 'Curl error: ' . curl_error( $resCurl );
                curl_close( $resCurl );
            } else {
                $result = curl_exec( $resCurl );
                curl_close( $resCurl );
                //echo $result;
            }

        endif;

    endif;

    $log_file       = THEME_DIR . '/admin/entrata/log-contact.txt';
    $log_message    = 'Contact form submission on ' . date("F j, Y, g:ia") .' UTC - from ['.$first_name.' '.$last_name.', Phone:'.$phone.', Email: '.$email.']'.PHP_EOL;
    $log_message   .= $result . PHP_EOL;
    file_put_contents( $log_file, $log_message, FILE_APPEND );

}
add_action( 'wpcf7_before_send_mail', 'entrata_contact_api' );


function tamaha_skip_mail( $f ) {

    $submission = WPCF7_Submission::get_instance();
    $data = $submission->get_posted_data();

    if ( 45 == $data['_wpcf7'] ) :
        return true;
    endif;

}
add_filter( 'wpcf7_skip_mail', 'tamaha_skip_mail' );
