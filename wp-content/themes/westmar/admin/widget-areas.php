<?php
/**
 * Widget Areas
 *
 * This file contains the functions necessary to register the theme widget areas.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_widgets_init() {

    register_sidebar( array(
        'name'          => 'Blog Sidebar',
        'id'            => 'blog-sidebar',
        'before_widget' => '<div class="sidebar__widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="sidebar__widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer',
        'id'            => 'footer-one',
        'before_widget' => '<div class="footer__widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="footer__widget-title">',
        'after_title'   => '</h3>',
    ) );

}
add_action( 'widgets_init', 'wm_widgets_init' );
