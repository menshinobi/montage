<?php
/*****************************************************************************
*
*	This function will fetch the floorplan data from Entrata.
*
*****************************************************************************/
/*
*   Check for and schedule the cron job, if it doesn't exist.
*/
if( !wp_next_scheduled( 'wm_fetch_floorplans_cron') ) {
	wp_schedule_event( time(), 'hourly', 'wm_fetch_floorplans_cron' );
}

add_action( 'wm_fetch_floorplans_cron',  'wm_fetch_floorplans_data' );

/*
*   Make the connection to Entrata and update the JSON file.
*/
function wm_fetch_floorplans_data() {

	$entrata_file 		= THEME_DIR . '/partials/entrata/floor-plans.json';

	/**
	 * JSON Request to Entrada.
	 */
	$jsonRequest =
	'{
	    "auth": {
	        "type" : "basic",
	        "username": "sam.winn",
	        "password": "Gnomes11"
	    },
	    "method": {
	        "name": "getMitsPropertyUnits",
	        "params": {
	            "propertyIds" : "120554",
	            "availableUnitsOnly" : "0",
	            "usePropertyPreferences" : "1",
	            "includeDisabledFloorplans" : "0",
	            "includeDisabledUnits" : "0",
	            "showUnitSpaces" : 1
	        }
	    }
	}';

	$resCurl = curl_init();
	curl_setopt( $resCurl, CURLOPT_HTTPHEADER, array( 'Content-type: APPLICATION/JSON; CHARSET=UTF-8' ) );
	curl_setopt( $resCurl, CURLOPT_POSTFIELDS, $jsonRequest );
	curl_setopt( $resCurl, CURLOPT_POST, true );
	curl_setopt( $resCurl, CURLOPT_URL, 'https://cardinal.entrata.com/api/propertyunits' );
	curl_setopt( $resCurl, CURLOPT_RETURNTRANSFER, 1);

	if ( curl_exec( $resCurl ) === false ) :
	    echo 'Curl error: ' . curl_error( $resCurl );
	    curl_close( $resCurl );
	else :
	    $result = curl_exec( $resCurl );
	    curl_close( $resCurl );
	endif;

	$data 		= json_decode($result);
	$floorplans = $data->response->result->PhysicalProperty->Property[0]->Floorplan;

	/**
	 * Save the data to the Entrata file.
	 */
	file_put_contents( $entrata_file, json_encode( $floorplans) );




	// Read the floor plans data and update the Price for each floorplan.
	$floorplans_json 	= json_decode( file_get_contents($entrata_file), TRUE );

	$args = array(
		'post_type' 		=> 'floor-plan',
		'posts_per_page'	=> -1,
	);

	$floorplans = new WP_Query( $args );
	if ( $floorplans->have_posts() ) :

		while ( $floorplans->have_posts() ) : $floorplans->the_post();

			$current_price = get_post_meta( $post->ID, 'floor_plan_price', true );

			foreach ( $floorplans_json as $unit => $value ) :

				$title 		= $value['Name'];
				$rent_price = $value['MarketRent']['@attributes']['Min'];

				if ( $title === get_the_title() ) :
					update_post_meta( get_the_ID(), 'floor_plan_price', $rent_price, $current_price );
				endif;

			endforeach;

		endwhile;
		wp_reset_postdata();

	endif;

	$log_file 		= THEME_DIR . '/partials/entrata/log.txt';
	$log_message 	= 'Retrieved Entrata data - ' . date("F j, Y, g:ia") .' UTC'.PHP_EOL;
	file_put_contents( $log_file, $log_message );

}