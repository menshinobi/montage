<?php

/**
 * Theme images
 *
 * Generates the URL to theme images directory.
 *
 * @package WordPress
 * @since 1.0
 */
function theme_image_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
		),
		$atts, 'theme_image'
	);

	return THEME_URL . '/assets/img/';
}
add_shortcode( 'theme_image', 'theme_image_shortcode' );





/**
 * Social Media Links
 *
 * Generates the output for the Westmar Social Media links.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_social_media_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
			'style' => 'text',
		),
		$atts, 'wm_social_links'
	);

	ob_start();

	$facebook_url 	= get_option( 'options_facebook' );
	$instagram_url 	= get_option( 'options_instagram' );
	$twitter_url 	= get_option( 'options_twitter' );
	$snapchat_url 	= get_option( 'options_snapchat' );
	$phone_number 	= get_option( 'options_phone_number' );

	$social_links = array(
		'facebook' 	=> $facebook_url,
		'twitter' 	=> $twitter_url,
		'instagram' => $instagram_url,
		'snapchat' 	=> $snapchat_url,
		'phone' 	=> $phone_number,
	);

	$class = 'wm-socials--' . $atts['style'];

	if ( ! empty( $social_links ) ) : ?>

		<ul class="wm-socials <?php echo $class; ?>">

	<?php endif; ?>

	<?php
	foreach ( $social_links as $key => $value ) :
		if ( ! empty( $value ) ) :

			if ( 'icons' == $atts['style'] ) :

				if( 'snapchat' == $key ) :
					echo '<li class="' . $key . '"><a href="#" target="_blank" class="open-modal" data-modal-id="snapchat"><img src="' . THEME_URL . '/assets/img/icon-' . $key .'.svg" alt="' . $key .' icon"></a></li>';
				elseif( 'phone' == $key ) :
				else :
					echo '<li class="' . $key . '"><a href="' . $value . '" target="_blank"><img src="' . THEME_URL . '/assets/img/icon-' . $key .'.svg" alt="' . $key .' icon"></a></li>';
				endif;

			else :

				if( 'snapchat' == $key ) :
					echo '<li class="' . $key . '"><a class="open-modal" data-modal-id="snapchat" href="#" target="_blank">' . $key . '</a></li>';
				elseif( 'phone' == $key ) :
					$number = str_replace( array( '(', ')', ' ', '-' ), '', $value);
					echo '<li class="' . $key . '"><a href="tel:' . $number . '">' . $value . '</a></li>';
				else :
					echo '<li class="' . $key . '"><a href="' . $value . '" target="_blank">' . $key . '</a></li>';
				endif;

			endif;

		endif;
	endforeach;
	?>

	</ul>

	<?php
	$shortcode_content = ob_get_clean();
	return $shortcode_content;
	?>

<?php
}
add_shortcode( 'wm_social_links', 'wm_social_media_shortcode' );