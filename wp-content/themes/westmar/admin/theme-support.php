<?php
/**
 *  This file adds any necessary theme
 *  functionality that does not require
 *  to be isolated in it's own file.
 *
 *  @package WordPress
 *  @since 1.0
 */

/**
 * Disable the visual editor
 */
add_filter( 'user_can_richedit' , '__return_false', 50 );

/**
 * Allow shortcodes in text widgets.
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Add thumbnail support.
 */
add_theme_support( 'post-thumbnails' );

/**
 * Add HTML5 Search compatibility.
 */
function wm_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wm_after_setup_theme' );


/**
 * Redirect Specific Post Types
 *
 * This function will disable the individual posts
 * from being viewed.
 *
 * @package WordPress
 * @since 1.0
 */
function wm_single_redirect() {

    $redirect_post_type = array( 'notifications', 'slides' );
    if ( is_singular( $redirect_post_type ) ) :
        wp_redirect( home_url() );
        exit();
    endif;

}
add_action( 'template_redirect', 'wm_single_redirect' );

/**
 * Allow SVG files via Media Uploader
 */
function wm_svg_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'wm_svg_mime_types' );

function wm_show_svg_in_media_library() {
	$css = '';
	$css = 'table.media .column-title .media-icon img[src$=".svg"] { width: 60px !important; height: auto !important; }';
	echo '<style type="text/css">' . $css . '</style>';
}
add_action( 'admin_head', 'wm_show_svg_in_media_library' );

/**
 * Filter search results
 *
 * Filters the search results to show only specific post types.
 *
 * @package WordPress
 * @since 1.0
 * @param array $query - current query.
 */
function filter_search_results( $query ) {

	if ( $query->is_search ) {
		$query->set( 'post_type',array( 'post', 'page' ) );
	}
	return $query;

}
add_filter( 'pre_get_posts','filter_search_results' );

/**
 * Remove Contact Form 7 Scripts and styles
 * Load them on the page templates that need them manually.
 */
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

/**
 * ADD PAGE/POST SLUG TO BODY CLASSES ARRAY
 *
 *  @param string $classes adds classes to the body_class array.
 */
/**
 * ADD PAGE/POST SLUG TO BODY CLASSES ARRAY
 *
 *  @param string $classes adds classes to the body_class array.
 */
function wm_add_slug_body_class( $wp_classes, $classes ) {

	global $post;
	global $template;

	if ( isset( $post ) ) :

		// Post Classes.
		if ( is_single() ) :
			$classes[] = 'post';
			$classes[] = 'single';
			$classes[] = 'single-' . $post->post_type;
		endif;

		// Page Classes.
		if ( is_page() ) :
			$page_template = basename( $template, '.php' );
			$classes[] = 'page';
			$classes[] = 'page-' . $post->post_name;
			$classes[] = $page_template;
		endif;

		if ( is_search() ) :
			$classes[] = 'page';
			$classes[] = 'search';
		endif;

		if ( is_home() || is_front_page() ) :
			$classes[] = 'page';
			$classes[] = 'index';
		endif;

		if( is_archive() ) :
			$cat = get_category( get_query_var( 'cat' ) );
			$classes[] = 'page';
			$classes[] = 'archive';
			$classes[] = 'archive-' . $cat->slug;
			$classes[] = 'page-' . $post->post_name;
			$classes[] = 'post-type-archive';
			$classes[] = 'post-type-archive-' . $post->post_type;
		endif;

	endif;

	if ( is_404() ) :
		$classes[] = 'page';
		$classes[] = 'page-404';
	endif;

	$wp_classes = array_intersect( $wp_classes, $classes );
	return array_merge( $wp_classes, (array) $classes );

}
add_filter( 'body_class', 'wm_add_slug_body_class', 10, 2 );

/**
 * Add browser to body classes.
 *
 */
function wm_browser_classes($classes)
{
    // the list of WordPress global browser checks
    // https://codex.wordpress.org/Global_Variables#Browser_Detection_Booleans
    $browsers = ['is_iphone', 'is_chrome', 'is_safari', 'is_NS4', 'is_opera', 'is_macIE', 'is_winIE', 'is_gecko', 'is_lynx', 'is_IE', 'is_edge'];

    // check the globals to see if the browser is in there and return a string with the match
    $classes[] = join(' ', array_filter($browsers, function ($browser) {
        return $GLOBALS[$browser];
    }));
    return $classes;
}
// call the filter for the body class
add_filter('body_class', 'wm_browser_classes');

/**
 * Remove File Versions
 *
 * This will remove the file version numbers.
 *
 * @package WordPress
 * @since 1.0
 */
function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/**
 * CHANGE DEFAULT FOOTER TEXT
 */
function wm_footer_admin() {
  echo '<span style="text-transform: uppercase; font-size: 0.85em; letter-spacing: 0.2em;">Theme developed by <a style="text-decoration: none;font-weight: bold; color:#555;" target="_blank" href="http://agencyfifty3.com/">Agency Fifty3</a></span>';
}
add_filter( 'admin_footer_text', 'wm_footer_admin' );

function wm_remove_menu() {
	remove_menu_page('edit.php');
}
add_action('admin_menu', 'wm_remove_menu');

/**
 * ADD STYLES FOR THE LOGIN PAGE
 */
function wm_login_styles() {

    $brand_logo               = THEME_URL . '/assets/img/logo.png';
    $brand_color              = '#6caeab';
    $button_text              = '#6caeab';
    $button_background        = '#ffffff';
    $button_background_hover  = '#6caeab';
    $input_label_color        = '#3f5364';

    echo '<style type="text/css">
    body { background: white; }
    h1 a { background-image:url('.$brand_logo.') !important; margin-top: 50px !important; background-size: 250px !important; width: 250px !important; height: 175px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
    #login { padding-top: 5% !important; width: 425px; }
    .login form { border: none; box-shadow:none; background: transparent;}
    .login form .input, .login input[type=text] {color: #6caeab; background: transparent; border:0;border-bottom: 1px solid #ccc;box-shadow:none;}
    input[type="submit"],
    .wp-core-ui .button-primary { padding: 15px 40px; background: '.$button_background.'; border: 2px solid #6caeab;border-radius: 2px; box-shadow: none; text-shadow: none; color: '.$button_text.'; text-transform: uppercase; font-size: 0.85em; font-weight: bold; letter-spacing: 0.1em; -webkit-transition: all 0.25s ease; -o-transition: all 0.25s ease; -moz-transition: all 0.25s ease; transition: all 0.25s ease; }
    .wp-core-ui .button.button-large { height: 40px; line-height: 28px;padding: 0 22px 2px;}
    input[type="submit"]:hover,
    .wp-core-ui .button-primary:hover { background: '.$button_background_hover.'; border-color: #6caeab; }
    div#login { padding-top: 3% !important; }
    input[type="text"]:active,
    input[type="text"]:focus,
    .login form .input:active,
    .login form .input:focus,
    input[type="password"]:active,
    input[type="password"]:focus { border-color: '.$input_label_color.'; box-shadow: none; }
    .login label { color: '.$input_label_color.'; text-transform: uppercase; letter-spacing: 0.15em; font-size: 0.85em; }
    .login #backtoblog a, .login #nav a {color: '.$input_label_color.';}
    </style>';
}
add_action( 'login_head', 'wm_login_styles' );