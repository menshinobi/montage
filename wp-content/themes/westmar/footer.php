<?php
/**
 * Footer
 *
 * This file generates the footer output
 *
 * @package WordPress
 * @since 1.0
 */

$footer_text 	= get_option( 'options_footer_text' );
$footer_logo	= get_option( 'options_footer_logo' );
$sidebars 		= array( 'footer-one' );
$sidebar_count 	= 0;

foreach ( $sidebars as $sidebar ) :
	if ( is_active_sidebar( $sidebar ) ) :
		$sidebar_count++;
	endif;
endforeach;

?>

<footer class="footer footer--columns-<?php echo $sidebar_count; ?>" >

	<div class="container container--top">

		<?php foreach ( $sidebars as $sidebar ) : ?>
			<?php if ( is_active_sidebar( $sidebar ) ) : ?>
				<div class="footer__column">
					<div class="inner">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>

	</div>
	<!-- .container.top -->

	<?php if ( ! empty( $footer_text ) ) : ?>

		<div class="container container--bottom">

			<div class="footer__menu">
				<?php wm_footer_menu(); ?>
			</div><!-- .footer__menu -->

			<?php if ( ! empty( $footer_logo ) ) : ?>
				<div class="footer__logo">
				<?php
					$logo = wp_get_attachment_image( $footer_logo, 'large', false, array() );
					echo '<a href="/">' . $logo . '</a>';
				?>
				</div>
			<?php endif; ?>

			<div class="footer__copyright">
				<?php echo do_shortcode( $footer_text ); ?>
			</div>

		</div>
		<!-- .container.bottom -->

	<?php endif; ?>

	<div class="modal modal--snapchat" data-modal-id="snapchat">
		<div class="container">
			<?php echo wp_get_attachment_image( get_option( 'options_snapchat' ), 'full', false, array() ); ?>
		</div>

		<button id="close-modal" class="menu-icon hamburger hamburger--elastic is-active" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</div>

</footer>

<?php wp_footer(); ?>

</body>
</html>
