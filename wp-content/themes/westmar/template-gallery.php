<?php
/**
 * Template Name: Gallery (Images)
 *
 * This page contains necessary markup for the gallery page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<div class="page-wrap">

		<?php

		while ( have_posts() ) : the_post();

			/**
			 * Get the gallery video section.
			 */
			get_template_part( 'partials/components/gallery', 'video' );

			/**
			 * Get the gallery images section.
			 */
			get_template_part( 'partials/components/gallery', 'images' );

			/**
			 * Get the gallery tour section.
			 */
			get_template_part( 'partials/components/gallery', 'tour' );

			?>

		<?php
		/**
		 * Get page footer link section.
		 */
		get_template_part( 'partials/components/page', 'footer-link' );
		?>

		<?php endwhile; ?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
