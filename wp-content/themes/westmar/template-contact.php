<?php
/**
 * Template Name: Contact
 *
 * This page contains necessary markup for the contact page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();

/*
* This is needed to limit Contact Form files to the contact page only.
* This must be present for contact forms to function on contact page.
*/
if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
    wpcf7_enqueue_styles();
}

if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
	wpcf7_enqueue_scripts();
}

if ( have_posts() ) : ?>

	<div class="page-wrap">

		<?php

		while ( have_posts() ) : the_post();

			/**
			 * Get the contact page header.
			 */
			get_template_part( 'partials/components/page-header', 'contact' );

			/**
			 * Get the contact page faq.
			 */
			get_template_part( 'partials/components/contact', 'faq' );

			/**
			 * Get page footer link section.
			 */
			get_template_part( 'partials/components/page', 'footer-link' );

			?>

		<?php endwhile; ?>

	</div>
	<!-- END .page-wrap -->

<?php endif; ?>
<?php get_footer(); ?>
