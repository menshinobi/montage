<?php
/**
 * Theme Functions
 *
 * This file contains all necessary functionality
 * to make the theme operate.
 *
 * @package WordPress
 * @since 1.0
 */

if ( ! defined( 'THEME_DIR' ) && ! defined( 'THEME_URL' ) ) {
  define( 'THEME_DIR', get_template_directory() );
  define( 'THEME_URL', get_template_directory_uri() );
}

add_filter( 'show_admin_bar', '__return_false' );

/**
 * Require additional files
 */
require_once( THEME_DIR . '/admin/theme-support.php' );
require_once( THEME_DIR . '/admin/cleanup.php' );
require_once( THEME_DIR . '/admin/navigation.php' );
require_once( THEME_DIR . '/admin/widget-areas.php' );
require_once( THEME_DIR . '/admin/enqueue-scripts.php' );
require_once( THEME_DIR . '/admin/shortcodes.php' );
require_once( THEME_DIR . '/admin/post-types/register-post-types.php' );
require_once( THEME_DIR . '/admin/acf/register-acf.php' );
require_once( THEME_DIR . '/admin/cronjobs.php' );
require_once( THEME_DIR . '/admin/entrata/entrata-contact-api.php' );

/**
 * Optional functionality.
 */
require_once( THEME_DIR . '/admin/notifications/notifications.php' );
