<?php
/**
 * Template Name: Floor Plans (Entrata)
 *
 * This page contains necessary markup for the floor plans page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();
?>

<div class="floorplans">

	<div class="container">

		<div class="floorplans__menu">
			<a href="#" class="all-bedrooms is-active" data-category="all">Show All</a>
			<a href="#" class="2-bedrooms" data-category="2-bedrooms">Two</a>
			<a href="#" class="3-bedrooms" data-category="3-bedrooms">Three</a>
			<a href="#" class="4-bedrooms" data-category="4-bedrooms">Four</a>
		</div>


	<?php
		/**
		 * Loop through the floorplan post type and
		 *  add the data to the array.
		 */
		$categories = get_terms( 'floor-plan-category' );
		$modals 	= array();

		foreach ( $categories as $category ) :

			$args = array(
				'post_type' 		=> 'floor-plan',
				'posts_per_page'	=> -1,
				'order' 			=> 'DESC',
				'tax_query' => array(
					array(
						'taxonomy' => 'floor-plan-category',
						'field'    => 'slug',
						'terms'    => $category->slug,
					),
				),
			);

			$floorplans_posts = new WP_Query( $args );

			if ( $floorplans_posts->have_posts() ) : ?>

				<?php $z = 0; ?>
				<?php while ( $floorplans_posts->have_posts() ) : $floorplans_posts->the_post(); ?>

					<?php
					$modals[] = get_the_ID();
					$floor_plan_size		= get_post_meta( $post->ID, 'floor_plan_size', true );
					$virtual_tour_type 		= get_post_meta( $post->ID, 'virtual_tour_type', true );

					switch ($category->slug) {
						case '2-bedrooms':
							$visuals_title = '2 Bedroom <span>|</span> 2 Bathroom';
							break;

						case '3-bedrooms':
							$visuals_title = '3 Bedroom <span>|</span> 3 Bathroom';
							break;

						case '4-bedrooms':
							$visuals_title = '4 Bedroom <span>|</span> 4 Bathroom';
							break;

						default:
							$visuals_title = '2 Bedroom <span>|</span> 2 Bathroom';
							break;
					}

					if ( '4-bedrooms' == $category->slug ) :
						$active = ( 1 == $z ) ? 'is-active' : null;
						$item_id = ( 1 == $z ) ? 'id="' . $category->slug . '"' : null;
					else :
						$active = ( 0 == $z ) ? 'is-active' : null;
						$item_id = ( 0 == $z ) ? 'id="' . $category->slug . '"' : null;
					endif;
					?>
					<?php $name_class 	= strtolower( str_replace(' ', '-', get_the_title() ) ); ?>

					<div <?php echo $item_id; ?> class="floorplan__header floorplan__header--<?php echo $name_class; ?> <?php echo $active; ?>" data-plan="<?php echo $z; ?>" data-category="<?php echo $category->slug; ?>">

						<div class="floorplan__type" data-plan="<?php echo $i; ?>">
                            <div class="floorplan_type_left">
                                <div class="floorplan__title"><?php echo $visuals_title; ?></div>
                                <div class="floorplan__size">
                                    <?php if ( $category->slug == '2-bedrooms' ) : ?>
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                    <?php elseif ( $category->slug == '3-bedrooms' ) : ?>
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                    <?php elseif ( $category->slug == '4-bedrooms' ) : ?>
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                        <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate icon">
                                    <?php endif; ?>
                                    <span><?php echo number_format( $floor_plan_size ); ?> sq. ft.</span>
                                </div>
                            </div>
                            <div class="floorplan_type_right">
                                <div><span>$XXX/mo</span><a href="#" class="app_today">Apply Today</a></div>
                            </div>
						</div><!-- floorplan__type -->

						<div class="floorplan__visuals <?php echo $active; ?>"  data-plan="<?php echo $i; ?>">

							<div class="floorplan__image">
								<?php
								if ( has_post_thumbnail() ) :
									the_post_thumbnail( 'large', array( 'class' => 'open-modal', 'data-modal-id' => 'floorplan-' . get_the_ID() ) );
								endif;
								?>
							</div><!-- .floorplan__image -->

							<div class="floorplan__video">

								<?php if ( ! empty( $virtual_tour_type ) ) : ?>

									<?php if ( 'iframe' == $virtual_tour_type ) : ?>

										<?php echo get_post_meta( $post->ID, 'virtual_tour_iframe', true ); ?>

									<?php elseif ( 'oembed' == $virtual_tour_type ) : ?>

										<?php
										$iframe = get_field('virtual_tour_oembed');
										preg_match('/src="(.+?)"/', $iframe, $matches);
										$src = $matches[1];

										$params = array(
										    'controls'    => 0,
										    'hd'        => 1,
										    'autohide'    => 1
										);

										$new_src = add_query_arg($params, $src);
										$iframe = str_replace($src, $new_src, $iframe);
										$attributes = 'frameborder="0"';
										$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
										?>

										<div class="embed-container">
											<?php echo $iframe; ?>
										</div>

									<?php else : ?>

										<div class="no-video">
											<h5 style="color:white;"><span>360 Video Tour</span>Coming Soon</h5>
										</div>

									<?php endif; ?>

								<?php else : ?>

									<div class="no-video">
										<h5 style="color:white;"><span>360 Video Tour</span>Coming Soon</h5>
									</div>

								<?php endif; ?>

							</div>

						</div><!-- .floorplam__visuals -->

					</div><!-- .floorplan__header -->

				<?php $z++; ?>
				<?php endwhile; ?>

				<div class="floorplan-container floorplan-container--<?php echo $category->slug; ?> is-active" data-category="<?php echo $category->slug; ?>">

				<?php $i = 0; ?>
				<?php while ( $floorplans_posts->have_posts() ) : $floorplans_posts->the_post(); ?>

					<?php $name_class 	= strtolower( str_replace(' ', '-', get_the_title() ) ); ?>
					<?php $first_active = ( '2-bedrooms' == $data['category'] ) ? 'is-active' : null; ?>

					<?php
					if ( '4-bedrooms' == $category->slug ) :
						$selected = ( 1 == $i ) ? 'is-selected' : null;
					else :
						$selected = ( 0 == $i ) ? 'is-selected' : null;
					endif;
					?>

					<?php
					$title 					= get_the_title();
					$floor_plan_title		= get_post_meta( $post->ID, 'floor_plan_title', true );
					$floor_plan_subtitle	= get_post_meta( $post->ID, 'floor_plan_subtitle', true );
					$available 				= get_post_meta( $post->ID, 'floor_plan_availability', true );
					$floor_plan_url 		= get_post_meta( $post->ID, 'floor_plan_url', true );
					$price 					= get_post_meta( $post->ID, 'floor_plan_price', true );
					?>

					<div class="floorplan floorplan--<?php echo $name_class; ?> <?php echo $selected; ?>" data-plan="<?php echo $i; ?>"" data-category="<?php echo $category->slug; ?>">

						<div class="container">
							<div class="floorplan__content">

								<div class="inner">

									<div class="info">
										<h4 class="floorplan__title"><?php echo $floor_plan_title; ?></h4>
										<div class="floorplan__price">Starting at $<?php echo number_format( $price ); ?></div>
										<?php if ( ! empty( $floor_plan_subtitle ) ) : ?>
											<div class="floorplan__subtitle"><?php echo $floor_plan_subtitle; ?></div>
										<?php endif; ?>
									</div>

									<div class="amenities">
										<div class="floorplan__amenities"><?php the_content(); ?></div>
									</div>

								</div>

								<?php if ( $available ) : ?>
									<a class="floorplan__button" href="<?php echo $floor_plan_url; ?>" target="_blank">Apply Today</a>
								<?php else : ?>
									<span class="floorplan__button floorplan__button--sold-out">Sold Out</span>
								<?php endif; ?>

							</div>

						</div>
					</div><!-- .floorplan -->

				<?php $i++; ?>
				<?php endwhile; ?>

				</div><!-- .floorplan-container -->

			<?php endif; ?>

		<?php endforeach; ?>

	</div><!-- .container -->

</div><!-- .floorplans -->

<?php

if ( have_posts() ) :

	while ( have_posts() ) : the_post();

		/**
		 * Get page footer link section.
		 */
		get_template_part( 'partials/components/page', 'footer-link' );

	endwhile;

endif;

?>

<?php foreach ( $modals as $modal ) : ?>
	<div class="modal modal--floorplan" data-modal-id="floorplan-<?php echo $modal; ?>">

		<div class="container">
			<?php echo get_the_post_thumbnail( $modal, 'large' ); ?>
		</div>

		<button id="close-modal" class="menu-icon hamburger hamburger--elastic is-active" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>

	</div>
<?php endforeach; ?>

<?php get_footer(); ?>
