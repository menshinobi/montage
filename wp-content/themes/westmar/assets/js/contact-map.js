google.maps.event.addDomListener(window, 'load', init);

var icon = {
    path: "M20,10c0,1.4-0.2,2.6-0.6,3.5l-7.1,15.1c-0.2,0.4-0.5,0.8-0.9,1C10.9,29.9,10.5,30,10,30 c-0.5,0-0.9-0.1-1.3-0.4c-0.4-0.2-0.7-0.6-0.9-1L0.6,13.5C0.2,12.6,0,11.4,0,10c0-2.8,1-5.1,2.9-7.1C4.9,1,7.2,0,10,0 c2.8,0,5.1,1,7.1,2.9C19,4.9,20,7.2,20,10z M13.5,13.5c1-1,1.5-2.2,1.5-3.5s-0.5-2.6-1.5-3.5c-1-1-2.2-1.5-3.5-1.5 C8.6,5,7.4,5.5,6.5,6.5C5.5,7.4,5,8.6,5,10s0.5,2.6,1.5,3.5c1,1,2.2,1.5,3.5,1.5C11.4,15,12.6,14.5,13.5,13.5z",
    fillColor: '#0b0b0b',
    fillOpacity: 1,
    anchor: new google.maps.Point(10, 30),
    strokeWeight: 0,
};
function init() {

    var mapOptions = {
        zoom: 17,
        scrollwheel: false,
        scaleControl: false,
        mapTypeControl: false,
        center: new google.maps.LatLng(43.669381, -79.383036),
        mapTypeControl: false,
        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    };

    var mapElement = document.getElementById('map-canvas');
    var map = new google.maps.Map(mapElement, mapOptions);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(43.669381, -79.383036),
        map: map,
        icon: icon,
        title: 'WPST',
    });
}
