jQuery( document ).ready( function($) {

	// Menu link scroller
	$( function() {
		var smoothLink = $('.scroll a, a.scroll, .btn--scroll');
		$(smoothLink).on('click', function(e){
			e.preventDefault();
			$('body, html').animate({
		        scrollTop: eval($($(this).attr('href')).offset().top - 60)
		    }, 500);
		});
	});
	// END menu link scroller

	// Menu Search
	$( function() {

		var searchIcon 		= $('.menu-item--search'),
			searchModule 	= $('.search-module'),
			searchClose 	= $('.search-module .close'),
			searchInput  	= $('.search-module input[type="search"]');

		$( searchIcon ).on( 'click', function(e) {
			e.preventDefault();
			$(searchModule).toggleClass('search-module--is-active');
			$(searchInput).focus();
			$(searchInput).val('');
		});

		$(searchClose).on('click', function(e) {
			e.preventDefault();
			$('.search-module').removeClass('search-module--is-active');
		});

	});
	// END Menu Search

	// Mobile menu functionality.
	$( function(){
		var menuIcon = $('.header .menu-icon'),
			menuWrap = $('.header'),
			menuItem = $('.header.mobile-active .menu-item a');

		$(menuIcon).on('click', function(){
			$(this).toggleClass('is-active');
			$(menuWrap).toggleClass('mobile-active');
			$('#google_translate_element').removeClass('is-active');
		});

		//Remove classes so menu doesn't show when "back" clicked.
		$( menuItem ).on( 'click', function(e) {
			if( $(menuWrap).hasClass('mobile-active') ) {
				$( menuIcon ).removeClass('is-active');
				$( menuWrap ).removeClass('mobile-active');
			}
		});

	});
	// END Mobile menu functionality.

	//Mobile Menu Functionality
	$( function() {
		var header = $('.header');
		var subMenu = $('.header.mobile-active .menu-item-has-children a');

		$(subMenu).on( 'click', function(e){
			e.preventDefault();
			$(this).parent('li').siblings('.menu-item-has-children').removeClass('open-menu');

			if( $(this).closest('li.menu-item-has-children').hasClass('open-menu') ) {

			} else {
				$(this).parent('.menu-item-has-children').toggleClass('open-menu');
				e.preventDefault();
			}

		});

	});
	//END Mobile Menu Functionality



	$(function() {
		var translate 			= $('.translate a');
		var translateBar 		= $('#google_translate_element');
		var translateSelect 	= $('#google_translate_element select');

		$(translate).on('click', function(e) {
			e.preventDefault();
			$(translateBar).toggleClass('is-active');
		});

		$(translateSelect).on('change', function() {
			console.log('change.');
			$(translateBar).toggleClass('is-active');
		});

		$('body').on('change', '#google_translate_element select', function() {
			console.log('here');
			$(translateBar).toggleClass('is-active');
		});

	});

	// Sticky header on scroll.
	// $(window).on('scroll', function() {

	//     var scrollTop = $(this).scrollTop();

	//     $('.header').each(function() {

	//         var headerHeight 	= $(this).outerHeight();

	//         if ( (headerHeight) < scrollTop ) {
	//             $('.header').addClass('shrink');
	//         }

	//         if ( headerHeight > scrollTop ) {
	//         	$('.header').removeClass('shrink');
	//         }

	//     });

	// });
	// END

});






/*
* Change the menu item colors on scroll.
* This will turn menu items from white to dark.
* simply add the class "menu-dark" to the section that should
* trigger the menu to turn from white to dark items and
* fire the adaptiveMenu funtion on that page.
*/
(function($) {

	if( ! $('.menu-dark').length > 0 ) {
		return false;
	}

	var controller 		= new ScrollMagic.Controller();
	var el 				= $('.main-nav .menu-item a');
	var trigger 		= '.menu-dark';
	var windowHeight 	= $(window).height();

	adaptiveMenu(el, trigger);

	function adaptiveMenu(el, trigger) {

		for ( var i = 0; i < el.length ;  i++ ) {

			var xTop 		= getPosition(el[i]);
			var x 			= el[i];
			var triggerHook = xTop / windowHeight;

			changeColor( x, trigger, triggerHook);
		}

	}
	// adaptiveMenu

	function changeColor(element, trigger, triggerHook) {

		var menuItemChange = new ScrollMagic.Scene({
			triggerElement: trigger,
			duration: 0,
			reverse: true,
			triggerHook: triggerHook,
			offset: -10
		})
		.on('enter', function() {
			$(element).addClass('is-dark');
		}).on('leave', function() {
			$(element).removeClass('is-dark');
		})
		// .addIndicators()
		.addTo(controller);

	}

	function getPosition(element) {
	    var yPosition = 0;
	    while(element) {
	        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
	        element = element.offsetParent;
	    }
	    return yPosition;
	}

})(jQuery);
