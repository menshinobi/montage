jQuery( document ).ready( function($) {

	// Notifications
	$( function() {

		var notification 		= ('.notifications-bar');
		var closeNotification 	= $('.close-notifications');
		var cookieInterval 		= $('.notifications-bar').data('display-interval');
		var cookie 				= Cookies.get('notificationCookie');

		// Show the notification bar if no cookie is set.
		if ( cookie !== 'disabled' ) {
			setTimeout(function() {
				$('.notifications-bar').removeClass('notifications-bar--hidden');
			}, 500);
		}

		// When the user closes the bar...
		$( closeNotification ).on( 'click', function() {

			console.log(cookieInterval);

			if( 'all' !== cookieInterval ) {
				Cookies.set('notificationCookie', 'disabled', { expires: cookieInterval });
			}

			var height = $('.notifications-bar').outerHeight(true) * -1;
			$(this).parent('.notifications-bar').css({'margin-top' :  height});

			setTimeout(function() {
				$('.notifications-bar').remove();
			}, 500);

		});

	});
	//END Notificatons

});