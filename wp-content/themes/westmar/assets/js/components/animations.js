/*
* This file contains the functions that allow for fade in animations.
* To use it, the ScrollMagic.js library much be included.
* Add the class "fade" to any element the effect should be applied to.
* Then you can define the CSS for that element when "is-active" class
* is applied as a result if this script.
*/

jQuery(document).ready(function() {

	// Initialize functions.
	fadeInElements();

});




/*
* This function will animate the photo sections on
* the amenities page.
*/
(function($) {

	if( ! $('body').hasClass('template-amenities') ) {
		return false;
	}

	var controller 		= new ScrollMagic.Controller();
	var el 				= $('.amenity.amenity--photo');

	$(el).each(function() {

		var amenitiesScene = new ScrollMagic.Scene({
			triggerElement: this,
			duration: 0,
			reverse: false,
			triggerHook: 0.7,
		})
		.setClassToggle(this, 'is-active')
		// .addIndicators()
		.addTo(controller);

	});

})(jQuery);





/*
* This function will animate the feature sections on
* the home page and location page.
*/
(function($) {

	if( ! $('body').hasClass('template-home') && ! $('body').hasClass('template-location') ) {
		return false;
	}

	var controller 		= new ScrollMagic.Controller();
	var el 				= $('.wm-feature');

	$(el).each(function() {

		var amenitiesScene = new ScrollMagic.Scene({
			triggerElement: this,
			duration: 0,
			reverse: false,
			triggerHook: 0.6,
		})
		.setClassToggle(this, 'is-active')
		// .addIndicators()
		.addTo(controller);

	});

})(jQuery);






/*
* This function will animate the hero section text
* on the home page and the grid icons.
*/
(function($) {

	if( ! $('body').hasClass('template-home') ) {
		return false;
	}

	var controller 		= new ScrollMagic.Controller();
	var el 				= $('.section-hero, .hero__text, .wm-icon-grid .item');

	$(el).each(function() {

		var amenitiesScene = new ScrollMagic.Scene({
			triggerElement: this,
			duration: 0,
			reverse: false,
			triggerHook: 0.75,
		})
		.setClassToggle(this, 'is-active')
		// .addIndicators()
		.addTo(controller);

	});

})(jQuery);



/*
* This function will animate the sticky header
* on the home page.
*/
(function($) {

	if( ! $('body').hasClass('template-home') ) {
		return false;
	}

	var controller 		= new ScrollMagic.Controller();
	var el 				= $('.slide__bar');
	var header			= $('.header.header--home');

	$(el).each(function() {

		var stickyheader = new ScrollMagic.Scene({
			triggerElement: this,
			duration: 0,
			offset: 60,
			reverse: true,
			triggerHook: 0.025,
		})
		.on( 'enter', function() {
			$('.header.header--home').addClass('is-active');
		})
		.on( 'leave', function() {
			$('.header.header--home').removeClass('is-active');
		})
		// .addIndicators()
		.addTo(controller);

	});

})(jQuery);




// Fade in Elements.
function fadeInElements() {

	var controller = new ScrollMagic.Controller();
	var el = document.querySelectorAll('.fade');

	for ( var i = 0; i < el.length ;  i++ ) {
		var x = el[i];
		fadeInElement( x, x, 0.85);
	}

	function fadeInElement(elTrigger, elAnimate, triggerHook) {
		var fadeScene = new ScrollMagic.Scene({
			triggerElement: elTrigger,
			duration: 100,
			reverse: false,
			triggerHook: triggerHook
		})
		.on('start', function() {
			var elTitle = elAnimate;
			if( jQuery(elTitle).hasClass('is-active') ) {
				jQuery(elTitle).removeClass('is-active');
			} else {
				jQuery(elTitle).addClass('is-active');
			}
		})
		.addIndicators()
		.addTo(controller);
	}
	// END - Fade in elements.

}
