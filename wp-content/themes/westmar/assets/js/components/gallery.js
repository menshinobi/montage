( function($) {

	var galleryCat 		= $('.gallery-images__categories .gallery-category');
	var galleryItem 	= $('.gallery-item');

	$(galleryCat).on('click', function() {

		$(this).siblings('.gallery-category').removeClass('is-active');
		$(this).addClass('is-active');

		var activeCategory 	= $(this).data('category');
		var nonce 			= $(this).data('nonce');

		if( 'all' == activeCategory ) {
			$(galleryItem).addClass('is-visible');
			return false;
		}

		$(galleryItem).each(function(){

			var category = $(this).data('category');
			if( activeCategory == category ) {
				$(this).addClass('is-visible');
			} else {
				$(this).removeClass('is-visible');
			}

		});

	});

	function initGalleryCarousel() {

		var gImage 		= $('.gallery-item--image.is-visible');
		var gModal 		= $('.gallery-modal');
		var gCarousel 	= $('.gallery-modal__carousel');

		// Open the modal.
		$(gImage).on('click', function() {
			$(this).addClass('is-active');
			$(gModal).addClass('is-active');
			$('.page-wrap').addClass('modal-active');
			var firstSlide = $('.gallery-item.is-visible').index(this);
			newGalleryCarousel(firstSlide);
		});

		// Close the modal.
		$('#close-gallery-modal').on('click', function() {
			$(gModal).removeClass('is-active');
			$(gImage).removeClass('is-active');
			$('.page-wrap').removeClass('modal-active');
			destroyGalleryCarousel();
		});

		// Launch the Carousel modal.
		function newGalleryCarousel(firstSlide) {

			$(gImage).each(function() {

				if( $(this).hasClass('is-visible') ) {
					var image = $(this).data('image');
					$(gCarousel).append('<div class="carousel-slide"><img src="' + image + '" alt=""></div>');
				}

			});

			// Initiate the carousel.
			$('.gallery-modal__carousel').slick({
		        autoplay: false,
		        autoplaySpeed: 5000,
		        fade: true,
		        dots: false,
		        slide: '.carousel-slide',
		        useTransform: true,
		        infinite: true,
		        slidesToShow: 1,
		        vertical: false,
		        speed: 320,
		        accessibility: true,
		        cssEase: 'cubic-bezier(0.600, -0.175, 0.7, 0.1)',
		        cssEase: 'ease-in',
		        nextArrow: '.gallery-modal__controls .next',
		        prevArrow: '.gallery-modal__controls .previous',
			});

			// Open the slide image that was clicked.
			if ( firstSlide ) {
				$('.gallery-modal__carousel').slick('slickGoTo', firstSlide, 0);
			}

			$(document).on('keydown', function(e) {
                if(e.keyCode == 37) {
                    $('.gallery-modal__carousel').slick('slickPrev');
                }
                if(e.keyCode == 39) {
                    $('.gallery-modal__carousel').slick('slickNext');
                }
            });

		}

		// Reset the carousel to its default state.
		function destroyGalleryCarousel() {
			$('.gallery-modal__carousel').slick('unslick');
			$('.gallery-modal__carousel').html('');
		}


	}
	initGalleryCarousel();






	function initModal() {

		var modal 		= $('.modal');
		var openModal 	= $('.open-modal');

		$(openModal).on('click', function(e) {

			e.preventDefault();
			var modalID = $(this).data('modal-id');

			$(modal).each(function() {

				var thisID = $(this).data('modal-id');

				if ( thisID && thisID == modalID ) {
					$(this).addClass('is-active');
					$('.page-wrap').addClass('modal-active');

					if( $(this).hasClass('modal--video') ) {
						var video = Wistia.api(thisID);
						if ( video !== null ) {
							video.play();
						}
					}

					return false;

				}

			});

		});

		// Close the modal.
		$('.close-modal, #close-modal').on('click', function() {

			$(modal).each(function() {
				$(this).removeClass('is-active');

				if( $(this).hasClass('modal--video') ) {
					var thisID = $(this).data('modal-id');
					var video = Wistia.api(thisID);
					if ( video !== null ) {
						video.pause();
					}

					if( $(this).hasClass('modal--gallery-video-oembed') ) {
						var videoEl = $(this).find('iframe');
						var videoSrc = $(this).find('iframe').attr('src');

						$(videoEl).attr('src', '');
						$(videoEl).attr('src', videoSrc);
					}

				}

			});

			$('.page-wrap').removeClass('modal-active');

		});
	}
	initModal();

})(jQuery);
