jQuery( document ).ready( function($) {

	// Redirect to the Thank You page on success.
	$('.wpcf7').on('wpcf7:mailsent', function(event){
		location = '/thank-you/';
	});

	(function($){

		var showAmenities 	= $('.amenities-chart .btn');
		var amenitiesChart 	= $('.amenities-chart__image--mobile');

		$(showAmenities).on('click', function() {

			if( $(amenitiesChart).hasClass('is-active') ) {
				$(amenitiesChart).removeClass('is-active');
				$(this).html('View Amenity Chart');
			} else {
				$(this).html('Hide Amenity Chart');
				$(amenitiesChart).addClass('is-active');
			}

		});

	})(jQuery);


	(function($) {

		var page 	= window.location;
		var bedType = window.location.hash.substr(1);
		var options = $('.floorplans__menu a');

		if( page.href.indexOf('floor-plans') <= -1 ) {
			return false; // Exit this function if we're not on the floorplans page.
		}

		if( ! bedType ) {
			return false;
		}

	})(jQuery);

	/*
	* Load the floor plan header when
	* a new unit is selected.
	*/
	(function($) {

		if( ! $('body').hasClass('template-floor-plans') ) {
			return;
		}

		var floorplan 			= $('.floorplan');
		var floorplanHeader 	= $('.floorplan__header');

		$(floorplan).on('click', function(e) {
			e.preventDefault();
			var floorplanId 	= $(this).data('plan');
			var floorCategory 	= $(this).data('category');
			var currentActive 	= $('.floorplans__menu .is-active').data('category');

			$(this).siblings('.floorplan').removeClass('is-selected');
			$(this).addClass('is-selected');

			$(floorplanHeader).each(function() {

				// When the "Show All" is selected.
				if( 'all' == currentActive ) {

					if( floorCategory == $(this).data('category') ) {

						if( floorplanId == $(this).data('plan') ) {
							$(this).addClass('is-active');
						} else {
							$(this).removeClass('is-active');
						}

					}

				} else if ( floorCategory == $(this).data('category') ) {

					if( floorplanId == $(this).data('plan') ) {
						$(this).addClass('is-active');
					} else {
						$(this).removeClass('is-active');
					}

				}

			});

		});

	})(jQuery);


	/*
	* Floor plans filtering by menu selection.
	*/
	(function($){

		var menuItems 			= $('.floorplans__menu a');
		var floorplan 			= $('.floorplan');
		var floorplanHeader 	= $('.floorplan__header');
		var floorplanContainer 	= $('.floorplan-container');

		$(menuItems).on('click', function() {

			var choice = $(this).data('category');
			$('.floorplans').attr('class', 'floorplans');
			$('.floorplans').addClass(choice);

			$(menuItems).removeClass('is-active');
			$(this).addClass('is-active');

			// When "Show All is clicked"
			if( 'all' == choice ) {
				showAllFloorplans();
				return false;
			}

			// Display the approprirate container.
			$(floorplanContainer).removeClass('is-active');
			$(floorplanContainer).each( function() {

				var category = $(this).data('category');
				if( category == choice ) {
					$(this).addClass('is-active');
				}

			});


			// Floorplan header sections.
			$(floorplanHeader).removeClass('is-active');
			$(floorplanHeader).each( function() {

				var category = $(this).data('category');

				if( category == choice ) {

					if( category == '4-bedrooms') {
						$('.floorplan__header--4-bed-4-bath-standard').addClass('is-active');
						$('.floorplan__header--4-bed-4-bath-elite').removeClass('is-active');
						$('.floorplan--4-bed-4-bath-standard').addClass('is-selected');
						$('.floorplan--4-bed-4-bath-elite').removeClass('is-selected');
						return false;
					} else if( category == '2-bedrooms' ) {
						$('.floorplan__header--2-bed-2-bath-standard').addClass('is-active');
						$('.floorplan__header--2-bed-2-bath-elite').removeClass('is-active');
						$('.floorplan--2-bed-2-bath-standard').addClass('is-selected');
						$('.floorplan--2-bed-2-bath-elite').removeClass('is-selected');
					} else if( category !== '4-bedrooms') {
						$(this).addClass('is-active');
						return false;
					}
				}

			});
			// END - Floorplan header sections.

			$(floorplan).removeClass('is-active');
			$(floorplan).each(function() {

				var bedrooms = $(this).data('category');
				var count = 0;
				if( bedrooms == choice ) {
					$(this).addClass('is-active');

					if( $(this).hasClass('floorplan--4-bed-4-bath-standard') ) {
						$(this).addClass('is-selected');
					}

					if( $(this).hasClass('floorplan--4-bed-4-bath-elite') ) {
						$(this).removeClass('is-selected');
					}


				}

			});

		});

		function showAllFloorplans() {

			// Container
			$(floorplanContainer).addClass('is-active');

			// Header
			$(floorplanHeader).each(function() {
				if( $(this).data('plan') == 0 ) {
					$(this).addClass('is-active');
				}
			});

			// Fixes.
			$('.floorplan--4-bed-4-bath-elite').removeClass('is-selected');
			$('.floorplan__header--4-bed-4-bath-elite').removeClass('is-active');
			$('.floorplan__header--4-bed-4-bath-standard').addClass('is-active');


		}

	})(jQuery);


});