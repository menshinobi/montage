<?php
/**
 * Template Name: Specials
 *
 * This page contains necessary markup for the specials page.
 *
 * @package WordPress
 * @since 1.0
 */

get_header();

if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<div class="page-wrap">

		<div class="page-content">

			<div class="container">
				<?php the_content(); ?>

				<div class="conditions">*Rules and Regulations apply. Please contact the leasing office for more details.</div>
			</div>

		</div><!-- .page-content -->

	</div>
	<!-- END .page-wrap -->

	<?php

	/**
	 * Get page footer link section.
	 */
	get_template_part( 'partials/components/page', 'footer-link' );

	?>

	<?php endwhile; ?>

<?php endif; ?>
<?php get_footer(); ?>
