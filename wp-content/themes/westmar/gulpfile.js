var gulp          = require( 'gulp' ),
    plumber       = require( 'gulp-plumber' ),
    watch         = require( 'gulp-watch' ),
    livereload    = require( 'gulp-livereload' ),
    cleanCSS      = require( 'gulp-clean-css' ),
    concat        = require( 'gulp-concat' ),
    jshint        = require( 'gulp-jshint' ),
    stylish       = require( 'jshint-stylish' ),
    uglify        = require( 'gulp-uglify' ),
    rename        = require( 'gulp-rename' ),
    notify        = require( 'gulp-notify' ),
    include       = require( 'gulp-include' ),
    sass          = require( 'gulp-sass' ),
    autoprefixer  = require( 'gulp-autoprefixer' );

var onError = function( err ) {
  console.log( 'An error occurred:', err.message );
  this.emit( 'end' );
}

var PATHS = {
  sass: [
    'node_modules/foundation-sites/scss/',
  ],
  javascript: [

    // Include your own custom scripts (located in the custom folder)

    // Animation libraries.
    // 'node_modules/gsap/src/minified/TweenLite.min.js',
    // 'node_modules/gsap/src/minified/TimelineLite.min.js',
    // 'node_modules/gsap/src/minified/plugins/CSSPlugin.min.js',
    // 'node_modules/gsap/src/minified/easing/EasePack.min.js',
    'node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
    // 'node_modules/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js',
    // 'node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js',

    // Theme necessities.
    'assets/js/slick.min.js',
    'assets/js/js-cookies.js',

    'assets/js/components/menu.js',
    'assets/js/components/gallery.js',
    'assets/js/components/animations.js',

    'assets/js/app.js',
  ]
};

gulp.task( 'scss', function() {
    return gulp.src( 'assets/scss/app.scss' )
    .pipe( plumber( { errorHandler: onError } ) )
    .pipe( sass({ includePaths : PATHS.sass }) )
    .pipe( autoprefixer('last 3 version') )
    .pipe( gulp.dest( './assets/css/' ) )
    .pipe( cleanCSS({compatibility: 'ie8'}))
    .pipe( rename( { suffix: '.min' } ) )
    .pipe( gulp.dest( './assets/css/min/' ) )
    .pipe( livereload() );
} );

gulp.task( 'javascript', function() {
  return gulp.src(PATHS.javascript)
  .pipe( plumber( { errorHandler: onError } ) )
  .pipe( concat('app.js') )
  .pipe( uglify() )
  .pipe( rename({ suffix: '.min' }) )
  .pipe( gulp.dest('./assets/js/min/') );
});

gulp.task( 'watch', function() {
  livereload.listen();
  gulp.watch( './assets/scss/**/*.scss', [ 'scss' ] );
  gulp.watch( './assets/js/*.js', [ 'javascript' ] );
  gulp.watch( './assets/js/components/*.js', [ 'javascript' ] );
  gulp.watch( './**/*.php' ).on( 'change', function( file ) {
    livereload.changed( file );
  } );
} );

gulp.task( 'default', [ 'scss', 'javascript', 'watch' ], function() {

} );