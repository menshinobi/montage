<?php
/**
 *	Template Name: Home
 *
 *	This template populates the any
 *	pages using the "Home" template.
 *
 *	@package WordPress
 *	@since 1.0
 */

get_header();
?>

<div class="page-wrap">

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			/**
			 * Hero Carousel
			 */
			get_template_part( 'partials/components/home', 'carousel-hero' );

			/**
			 * Home page section modules.
			 */
			get_template_part( 'partials/components/home', 'sections' );

			/**
			 * Get page footer link section.
			 */
			get_template_part( 'partials/components/page', 'footer-link' );

			?>

		<?php endwhile; ?>

	<?php endif; ?>

</div>
<!-- END .page-wrap -->

<?php get_footer(); ?>
