<div class="post">

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="post__image">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'large' ); ?>
			</a>
		</div><!-- .post__iamge -->
	<?php endif; ?>

	<a href="<?php the_permalink(); ?>"><h2 class="post__title"><?php the_title(); ?></h2></a>
	<div class="post__date"><?php echo get_the_date( 'M j, Y' ); ?></div>
	<div class="post__content">
		<?php if ( ! is_search() ) : ?>
			<?php $content = wp_trim_words( get_the_content(), 25, null ); ?>
			<?php echo wpautop( $content ); ?>
		<?php endif; ?>
	</div><!-- .post__content -->

</div>