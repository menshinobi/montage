<div id="google_translate_element">
	<div class="container">
		<script type="text/javascript">
			function googleTranslateElementInit() {
			  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
			}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	</div>
</div>

<div class="header-top">
	<div class="container">
		<?php wm_top_menu(); ?>
	</div>
</div>

<div class="header">

	<div class="container">
		<?php
		$wm_logo_id 	= get_option( 'options_logo' );
		$wm_logo 		= wp_get_attachment_image( $wm_logo_id, 'large', false, array( 'alt' => get_bloginfo( 'name' ) . ' Logo' ) );
		?>

		<nav class="menu">

			<?php if ( ! empty( $wm_logo ) ) : ?>
				<div class="logo">
					<a href="<?php echo bloginfo( 'url' ); ?>"><?php echo $wm_logo; ?></a>
				</div>
			<?php else : ?>
				<div class="logo">
					<a href="/"><h1><?php echo bloginfo( 'name' ); ?></h1></a>
				</div>
			<?php endif; ?>

			<?php wm_main_menu(); ?>

			<?php $phone_number = get_option( 'options_phone_number' ); ?>

			<?php if ( ! empty( $phone_number ) ) : ?>
				<?php $link = str_replace( array( '(', ')', ' ', '-' ), '', $phone_number); ?>
				<div class="menu-phone">
					<a href="tel:<?php echo $link; ?>"><?php echo $phone_number; ?></a>
				</div>
			<?php endif; ?>


			<button class="menu-icon hamburger hamburger--elastic" type="button">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>

		</nav>
		<!-- .menu -->
	</div>

</div>
<!-- .header -->


<?php if ( is_page_template( 'template-home.php' ) ) : ?>

	<div class="header header--home">

		<div class="container">
			<?php
			$wm_logo_id 	= get_option( 'options_logo' );
			$wm_logo 		= wp_get_attachment_image( $wm_logo_id, 'large', false, array( 'alt' => get_bloginfo( 'name' ) . ' Logo' ) );
			?>

			<nav class="menu">

				<?php if ( ! empty( $wm_logo ) ) : ?>
					<div class="logo">
						<a href="<?php echo bloginfo( 'url' ); ?>"><?php echo $wm_logo; ?></a>
					</div>
				<?php else : ?>
					<div class="logo">
						<a href="/"><h1><?php echo bloginfo( 'name' ); ?></h1></a>
					</div>
				<?php endif; ?>

				<?php wm_main_menu(); ?>
				<?php wm_top_menu(); ?>

				<button class="menu-icon hamburger hamburger--elastic" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>

			</nav>
			<!-- .menu -->

<!-- 			<div class="header-top">
				<div class="container">
					<?php wm_top_menu(); ?>
				</div>
			</div> -->

		</div>

	</div>
	<!-- .header -->

<?php endif; ?>
