<?php
$gallery_images_title 	= get_post_meta( $post->ID, 'gallery_images_title', true );
$gallery_categories 	= get_terms( 'gallery_category', array( 'hide_empty' => true ) );
?>

<div class="gallery-images">

	<div class="container">
		<?php if ( ! empty( $gallery_images_title ) ) : ?>
			<h3 class="gallery-images__title"><?php echo __( $gallery_images_title, 'wm' ); ?></h3>
		<?php endif; ?>
	</div>

	<?php if ( ! empty( $gallery_categories ) ) : ?>

	<div class="gallery-images__categories">

		<div class="container">

			<li class="gallery-category is-active" data-nonce="<?php echo wp_create_nonce('wm_secret'); ?>" data-category="all">Show All</li>

			<?php foreach ( $gallery_categories as $category ) : ?>
				<li class="gallery-category" data-nonce="<?php echo wp_create_nonce('wm_secret'); ?>" data-category="<?php echo $category->slug; ?>"><?php echo $category->name; ?></li>
			<?php endforeach; ?>

		</div><!-- .container -->

	</div><!-- .gallery-images__categories -->

	<?php endif; ?>

	<div class="gallery-grid">

		<div class="container">

		<?php
		foreach ( $gallery_categories as $category ) :

			$args = array(
				'post_type' 		=> 'gallery',
				'posts_per_page'	=>  -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'gallery_category',
						'field'    => 'slug',
						'terms'    => $category->slug,
					),
				),
			);

			$gallery_items = new WP_Query( $args );
			if ( $gallery_items->have_posts() ) : ?>

				<?php while ( $gallery_items->have_posts() ) : $gallery_items->the_post(); ?>

					<?php if ( 'videos' !== $category->slug ) : ?>

		                <?php $gallery_images = get_post_meta( get_the_ID(), 'gallery_images', true ); ?>

		                <?php if ( ! empty( $gallery_images ) ) : ?>

		                    <?php foreach ( $gallery_images as $image_id ) : ?>

		                        <?php $image_url = wp_get_attachment_url( $image_id, 'large', false ); ?>

		                            <div class="gallery-item gallery-item--image is-visible" data-category="<?php echo $category->slug; ?>" data-image="<?php echo $image_url; ?>">
		                                <span style="background-image: url('<?php echo $image_url; ?>');"></span>
		                            </div>

		                    <?php endforeach; ?>

		                <?php endif; ?>

		            <?php else : ?>

	                    <?php
	                    $type = get_post_meta( get_the_ID(), 'video_type', true );
	                    $thumbnail = get_post_meta( get_the_ID(), 'video_thumbnail', true );
	                    $thumbnail_url = wp_get_attachment_url( $thumbnail, 'medium' );
	                    $style = ( ! empty( $thumbnail ) ) ? 'style="background-image: url(' . $thumbnail_url . ');"' : null;

				        	if ( 'oembed' == $type ) :
				        		$video = get_field('video_oembed');
				        		preg_match('/src="(.+?)"/', $video, $matches);
				        		$src = $matches[1];

				        		$params = array(
				        		    'controls'    => 0,
				        		    'hd'        => 1,
				        		    'autohide'    => 1
				        		);

				        		$new_src = add_query_arg($params, $src);
				        		$video = str_replace($src, $new_src, $video);
				        		$attributes = 'frameborder="0"';
				        		$video = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $video);
				        		?>

			                        <div class="gallery-item gallery-item--video open-modal is-visible" data-category="<?php echo $category->slug; ?>" data-modal-id="gallery-video-<?php echo get_the_ID(); ?>" <?php echo $style; ?>>
				                        <img src="<?php echo THEME_URL; ?>/assets/img/play-button.svg" alt="Play">
			                            <span></span>
			                        </div>

									<div class="modal modal--gallery-video modal--video modal--gallery-video-<?php echo $type; ?>" data-modal-id="gallery-video-<?php echo get_the_ID(); ?>">

										<div class="container">
											<?php if ( ! empty( $video ) ) : ?>
												<div class="gallery-video-iframe embed-container">
													<?php echo $video; ?>
												</div>
											<?php endif; ?>
										</div>

										<button id="close-modal" class="close-modal menu-icon hamburger hamburger--elastic is-active" type="button">
											<span class="hamburger-box">
												<span class="hamburger-inner"></span>
											</span>
										</button>

									</div>

		                        <?php

				        	elseif ( 'iframe' == $type ) :

				        		$video 		= get_post_meta( get_the_ID(), 'video_iframe', true );
				        		$video_id 	= get_post_meta( get_the_ID(), 'video_id', true );
					        	?>

		                        <div class="gallery-item gallery-item--video open-modal is-visible" data-category="<?php echo $category->slug; ?>" data-modal-id="<?php echo $video_id; ?>" <?php echo $style; ?>>
			                        <!-- <img src="<?php echo THEME_URL; ?>/assets/img/play-button.svg" alt="Play"> -->
		                            <span></span>
		                        </div>

								<div class="modal modal--gallery-video modal--video modal--gallery-video-<?php echo $type; ?>" data-modal-id="<?php echo $video_id; ?>">

									<div class="container">
										<?php if ( ! empty( $video ) ) : ?>
											<div class="gallery-video-iframe">
												<?php echo $video; ?>
											</div>
										<?php endif; ?>
									</div>

									<button id="close-modal" class="close-modal menu-icon hamburger hamburger--elastic is-active" type="button">
										<span class="hamburger-box">
											<span class="hamburger-inner"></span>
										</span>
									</button>

								</div>

                        <?php endif; ?>

					<?php endif; ?>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php endif; ?>

		<?php endforeach; ?>

		</div><!-- .container -->

	</div><!-- .gallery-grid -->

</div>



<div class="gallery-modal">

	<div class="container">

		<div class="gallery-modal__controls  gallery-modal__controls--previous">
			<div class="previous"><svg viewBox="0 0 36 18.5"><polygon points="18,18.7 0,0.7 0.7,0 18,17.2 35.2,0 35.9,0.7   "></polygon></svg></div>
		</div>

		<div class="gallery-modal__carousel"></div>

		<div class="gallery-modal__controls  gallery-modal__controls--next">
			<div class="next"><svg viewBox="0 0 36 18.5"><polygon points="18,18.7 0,0.7 0.7,0 18,17.2 35.2,0 35.9,0.7   "></polygon></svg></div>
		</div>

	</div>

	<button id="close-gallery-modal" class="menu-icon hamburger hamburger--elastic is-active" type="button">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
	</button>

</div>