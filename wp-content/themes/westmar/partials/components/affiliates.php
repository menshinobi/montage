<?php
$affiliates_title = get_post_meta( $post->ID, 'affiliates_title', true );
$affiliates = get_post_meta( $post->ID, 'affiliates', true );
?>

<?php if ( ! empty( $affiliates ) ) : ?>

	<div class="affiliates">
		<div class="container">
			<h4 class="affiliates__title"><?php echo __( $affiliates_title, 'wm' ); ?></h4>

			<?php for( $i = 0; $i < $affiliates; $i++ ) : ?>

					<?php $logo_id 		= get_post_meta( $post->ID, 'affiliates_' . $i . '_affiliates_logo', true ); ?>
					<?php $link 		= get_post_meta( $post->ID, 'affiliates_' . $i . '_affiliate_link', true ); ?>

					<div class="affiliate">
						<?php if ( ! empty( $link ) ) : ?><a target="_blank" href="<?php echo $link; ?>"><?php endif; ?>
							<div class="affiliate__logo"><img src="<?php echo wp_get_attachment_url( $logo_id, 'large'); ?>" alt=""></div>
						<?php if ( ! empty( $link ) ) : ?></a><?php endif; ?>
					</div>

			<?php endfor; ?>

		</div>
	</div><!-- affiliates -->


<?php endif; ?>