<?php
$feature_title 			= get_post_meta( $post->ID, 'feature_title', true );
$feature_image 			= get_post_meta( $post->ID, 'feature_image', true );
$feature_image_tall 	= get_post_meta( $post->ID, 'feature_image_tall', true );
$feature_image_small 	= get_post_meta( $post->ID, 'feature_image_small', true );

$class = ( 14 == get_the_ID() ) ? ' has-pattern-br' : null;
?>

<div class="wm-feature<?php echo $class; ?>">
	<div class="container">

		<div class="wm-feature__image-main">
			<?php echo wp_get_attachment_image( $feature_image, 'large', false, array() ); ?>
		</div>

		<div class="wm-feature__content">

			<h3 class="wm-feature__title"><?php echo __( $feature_title, 'wm' ); ?></h3>

			<div class="wm-feature__image wm-feature__image--tall">
				<?php echo wp_get_attachment_image( $feature_image_tall, 'large', false, array() ); ?>
			</div>

			<div class="wm-feature__image">
				<?php echo wp_get_attachment_image( $feature_image_small, 'large', false, array() ); ?>
			</div>

		</div>

	</div>
</div>