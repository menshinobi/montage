<?php
/**
 * Page Header.
 *
 * Generates the output for the header section of the page.php file.
 *
 * @package WordPress
 * @since 1.0
 */

if ( is_home() || is_front_page() ) :

	$blog_id 				= get_page_by_path( 'blog' );
	$page_title 			= '';
	$page_subtitle 			= '';
	$featured_image_url 	= get_the_post_thumbnail_url( $blog_id, 'full' );
	! empty( $featured_image_url ) ? $header_background = 'style="background: url(' . $featured_image_url . ') no-repeat center top / cover;"' : '';

elseif ( is_archive() ) :

	$page_title = get_the_archive_title();

else :

	$page_title = get_the_title();

endif;
?>

<div class="page-header" <?php echo $header_background; ?>>
	<div class="page-header__overlay"></div>

	<div class="container">
		<div class="page-header__content">
			<h1 class="page-header__title"><?php echo esc_html( $page_title ); ?></h1>
			<?php
			if ( ! empty( $page_subtitle ) ) :
				echo '<span class="page-header__subtitle">' . esc_html( $page_subtitle ) . '</span>';
			endif;
			?>
		</div>
		<!-- .page-header__content -->
	</div>

	<?php if ( is_home() || is_front_page() ) : ?>
		<div class="page-header__feed container-full">
			<?php get_template_part( 'partials/components/post-feed' ); ?>
		</div>
	<?php endif; ?>

</div>
<!-- END .page-header -->
