<?php
$unit_amenities 		= get_post_meta( $post->ID, 'unit_amenities', true );
$unit_amenities_title 	= get_post_meta( $post->ID, 'unit_amenities_title', true );

if ( $unit_amenities ) : ?>

	<div class="amenities-list">

		<div class="container">

			<div class="amenities-list__title"><span><?php echo __( $unit_amenities_title, 'wm' ); ?></span><span class="dot-line"></span></div>

		<?php
		for( $i = 0; $i < $unit_amenities; $i++ ) :
			$amenity_title 		= get_post_meta( $post->ID, 'unit_amenities_' . $i . '_amenity_title', true );
			$icon_id 			= get_post_meta( $post->ID, 'unit_amenities_' . $i . '_amenity_icon', true );
			$amenity_icon_url	= wp_get_attachment_url( $icon_id, 'medium', array() ); ?>

			<div class="amenity">
				<div class="amenity__icon"><img src="<?php echo $amenity_icon_url; ?>"></div>
				<div class="amenity__title"><?php echo __( $amenity_title, 'wm' ); ?></div>
			</div>

		<?php endfor; ?>



		</div>

	</div>

<?php endif; ?>
