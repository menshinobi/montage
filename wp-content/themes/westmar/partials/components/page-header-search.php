<?php
/**
 * Page Header.
 *
 * Generates the output for the header section of the page.php file.
 *
 * @package WordPress
 * @since 1.0
 */

$page_title = get_search_query();
?>

<div class="page-header">
	<div class="page-header__overlay"></div>

	<div class="container">
		<div class="page-header__content">
			<h1 class="page-header__title"><?php echo esc_html( $page_title ); ?></h1>
		</div><!-- .page-header__content -->
	</div>

</div><!-- END .page-header -->
