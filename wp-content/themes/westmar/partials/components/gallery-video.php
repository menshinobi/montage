<?php
$gallery_video_title 		= get_post_meta( $post->ID, 'gallery_video_title', true );
$gallery_video 				= get_post_meta( $post->ID, 'gallery_video_code', true );
$gallery_video_id			= get_post_meta( $post->ID, 'gallery_video_id', true );
$gallery_video_image_id 	= get_post_meta( $post->ID, 'gallery_video_image', true );
?>

<div class="gallery-video">
	<div class="container">
		<?php if ( ! empty( $gallery_video_title ) ) : ?>
			<h3 class="gallery-video__title"><?php echo __( $gallery_video_title, 'wm' ); ?></h3>
		<?php endif; ?>

		<?php if ( ! empty( $gallery_video_image_id ) ) : ?>
			<div class="gallery-video__image">
				<div class="gallery-video__overlay"></div>
				<?php echo wp_get_attachment_image( $gallery_video_image_id, 'full', false, array() ); ?>
				<span class="open-modal" data-modal-id="<?php echo $gallery_video_id; ?>"><img src="<?php echo THEME_URL; ?>/assets/img/play-button.svg" alt="Play"></span>
			</div>
		<?php endif; ?>
	</div>
</div>



<div class="modal modal--video" data-modal-id="<?php echo $gallery_video_id; ?>">

	<div class="container">
		<?php if ( ! empty( $gallery_video ) ) : ?>
			<div class="gallery-video-iframe">
				<?php echo $gallery_video; ?>
			</div>
		<?php endif; ?>
	</div>

	<button id="close-modal" class="menu-icon hamburger hamburger--elastic is-active" type="button">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
	</button>

</div>