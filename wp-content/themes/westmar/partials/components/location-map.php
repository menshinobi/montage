<?php
$map_content 	= get_post_meta( $post->ID, 'map_content', true );
$map_image_id 	= get_post_meta( $post->ID, 'map_image', true );
$map_link 		= get_post_meta( $post->ID, 'map_link', true );
$map_link_text 	= get_post_meta( $post->ID, 'map_link_text', true );
?>

<div class="wm-map">

	<div class="container">

		<?php if ( ! empty( $map_content ) ) : ?>
			<div class="wm-map__content">
				<?php echo wpautop( $map_content ); ?>
			</div>
		<?php endif; ?>
    </div>
		<?php if ( ! empty( $map_link ) && ! empty( $map_link_text ) ) : ?>
			<a href="<?php echo $map_link; ?>" target="_blank" class="btn wm-map__button"><?php echo __( $map_link_text, 'wm' ); ?></a>
		<?php endif; ?>

		<?php if ( ! empty( $map_image_id ) ) : ?>
			<div class="wm-map__image">
				<?php echo wp_get_attachment_image( $map_image_id, 'full', false, array() ); ?>
			</div>
		<?php endif; ?>



</div><!-- .wm-map -->
