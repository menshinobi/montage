<?php
$photo_amenities		= get_post_meta( $post->ID, 'photo_amenities', true );
?>

<div class="amenities-list">

	<div class="container">

	<?php
	$i = 0;
	foreach ( $photo_amenities as $content_type ) : ?>

		<?php
		if ( 'photo_amenities' == $content_type ) :

			$amenity_icon_id 		= get_post_meta( $post->ID, 'photo_amenities_' . $i . '_amenity_icon', true );
			$amenity_title 			= get_post_meta( $post->ID, 'photo_amenities_' . $i . '_amenity_title', true );
			$amenity_description 	= get_post_meta( $post->ID, 'photo_amenities_' . $i . '_amenity_description', true );
			$amenity_image_id		= get_post_meta( $post->ID, 'photo_amenities_' . $i . '_amenity_image', true );

		?>

					<div class="amenity amenity--photo">

						<div class="amenity__content">
							<div class="inner">
								<div class="amenity__icon">
									<?php if ( ! empty( $amenity_icon_id ) ) : ?>
										<?php echo wp_get_attachment_image( $amenity_icon_id, 'large', false, array() ); ?>
									<?php endif; ?>
								</div>
								<h3 class="amenity__title"><?php echo __( $amenity_title, 'wm' ); ?></h3>
								<div class="amenity__description"><?php echo $amenity_description; ?></div>
							</div>
						</div>

						<div class="amenity__image">
							<?php if ( ! empty( $amenity_image_id ) ) : ?>
								<?php echo wp_get_attachment_image( $amenity_image_id, 'large', false, array() ); ?>
							<?php endif; ?>
						</div>

					</div>

		<?php endif; ?>

	<?php $i++; ?>
	<?php endforeach; ?>

	</div>

</div><!-- amenities-list -->