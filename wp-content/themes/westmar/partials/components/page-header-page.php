<?php
/**
 * Page Header.
 *
 * Generates the output for the header section of the page.php file.
 *
 * @package WordPress
 * @since 1.0
 */

	// Header options data.
	$featured_image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
	$page_title 		= get_post_meta( $post->ID, 'header_title', true );
	$page_subtitle 		= get_post_meta( $post->ID, 'header_subtitle', true );
	! empty( $page_title ) ? $page_title = $page_title : $page_title = get_the_title();
	! empty( $featured_image_url ) ? $header_background = 'style="background: url(' . $featured_image_url . ') no-repeat center top / cover;"' : '';
?>

<div id="page-header" class="page-header" <?php echo $header_background; ?>>
	<div class="page-header__overlay"></div>

	<div class="page-header__content">
		<h1 class="page-header__title"><?php echo esc_html( $page_title ); ?></h1>
		<?php
		if ( ! empty( $page_subtitle ) ) :
			echo '<span class="page-header__subtitle">' . esc_html( $page_subtitle ) . '</span>';
		endif;
		?>
	</div>
	<!-- .page-header__content -->

</div>
<!-- END .page-header -->
