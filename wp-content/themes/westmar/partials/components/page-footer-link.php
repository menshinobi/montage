<?php
$page_id 		= get_post_meta( $post->ID, 'page_link', true );
$link_anchor 	= get_post_meta( $post->ID, 'link_text', true );
?>
<div class="page-footer-link menu-dark">
<div class="container">
	<a href="<?php the_permalink( $page_id ); ?>"><?php echo $link_anchor; ?></a>
</div>
</div>