<?php
$amenities_chart_title 			= get_post_meta( $post->ID, 'amenities_chart_title', true );
$amenities_chart_image_id 		= get_post_meta( $post->ID, 'amenities_chart_image', true );
$amenities_chart_pdf_id 		= get_post_meta( $post->ID, 'amenities_chart_pdf', true );
?>

<?php if ( ! empty( $amenities_chart_title ) ) : ?>

<div class="amenities-chart">
	<div class="container">

		<h3  id="amenities-chart" class="amenities-chart__title"><?php echo __( $amenities_chart_title, 'wm' ); ?></h3>

		<span class="btn">View Amenity Chart</span>

		<?php if ( ! empty( $amenities_chart_image_id ) ) : ?>
			<div class="amenities-chart__image amenities-chart__image--desktop is-active">

				<?php if ( ! empty( $amenities_chart_pdf_id ) ) : ?>
					<!-- <a href="<?php echo wp_get_attachment_url( $amenities_chart_pdf_id, 'full' ); ?>" class="download"><img class="is-svg" src="<?php echo THEME_URL; ?>/assets/img/icon-form.svg"><span>Download</span></a> -->
				<?php endif; ?>

				<?php echo wp_get_attachment_image( $amenities_chart_image_id, 'full', false, array() ); ?>

			</div>

			<div class="amenities-chart__image amenities-chart__image--mobile">

				<?php if ( ! empty( $amenities_chart_pdf_id ) ) : ?>
					<!-- <a href="<?php echo wp_get_attachment_url( $amenities_chart_pdf_id, 'full' ); ?>" class="download"><img class="is-svg" src="<?php echo THEME_URL; ?>/assets/img/icon-form.svg"><span>Download</span></a> -->
				<?php endif; ?>

				<?php echo wp_get_attachment_image( $amenities_chart_image_id, 'full', false, array() ); ?>

			</div>
		<?php endif; ?>

	</div>
</div>

<?php endif; ?>