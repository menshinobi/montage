<?php
/**
 * Page Header for "template-contact.php".
 *
 * Generates the output for the header section of the template-contact.php file.
 *
 * @package WordPress
 * @since 1.0
 */

	// Page header options.
	$contact_form_title 	= get_post_meta( $post->ID, 'contact_form_title', true );
	$contact_form 			= get_post_meta( $post->ID, 'contact_form', true );

?>

<div class="page-header">

	<div class="page-header__content">

		<?php if ( ! empty( $contact_form_title ) ) : ?>
			<h1 class="page-header__title"><?php echo esc_html( __( $contact_form_title, 'wm' ) ); ?></h1>
		<?php endif; ?>

		<?php
		if ( ! empty( $contact_form ) ) :
			$contact_form = '[contact-form-7 id="' . $contact_form . '" title="' . get_the_title( $contact_form ) . '"]';
			echo do_shortcode( $contact_form );
		endif;
		?>

	</div>
	<!-- .page-header__content -->

</div>
<!-- END .page-header -->
