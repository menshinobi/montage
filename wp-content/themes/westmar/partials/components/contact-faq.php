<?php
$faq_section 			= get_post_meta( $post->ID, 'faq_section', true );
$faq_title 				= get_post_meta( $post->ID, 'faq_title', true );
$contact_information 	= get_post_meta( $post->ID, 'contact_information', true );
?>

<?php if ( true == $faq_section ) : ?>

<div class="wm-faq menu-dark">

	<a class="wm-faq__scroll scroll" href="#faq">frequently asked questions <img src="<?php echo THEME_URL; ?>/assets/img/arrow-header.svg" alt="arrow"></a>

	<div class="container">
		<h2 id="faq" class="wm-faq__title"><?php echo __( $faq_title, 'wm' ); ?></h2>
	</div>

	<div class="container">

	<?php
	$args = array(
		'post_type' 		=> 'faqs',
		'posts_per_page'	=> -1,
	);

	$wm_faq = new WP_Query( $args );
	if ( $wm_faq->have_posts() ) :
	?>

		<ul class="faqs">

		<?php while ( $wm_faq->have_posts() ) : $wm_faq->the_post(); ?>

			<li class="faq">
				<div class="faq__icon">
					<?php
					if ( has_post_thumbnail() ) :
						the_post_thumbnail( 'medium' );
					endif;
					?>
				</div>
				<div class="faq__content">
					<div class="faq__title"><?php the_title(); ?></div>
					<div class="faq__answer"><?php the_content(); ?></div>
				</div>
			</li><!-- .faq -->

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>

		</ul><!-- .faqs -->

	<?php endif; ?>


		<div class="wm-contact">
			<img class="icon" src="<?php echo THEME_URL; ?>/assets/img/icon-person.png" alt="Person Icon">
			<?php echo wpautop( $contact_information ); ?>
			<?php echo do_shortcode( '[wm_social_links style="icons"]' ); ?>
		</div>

	</div>

</div><!-- .wm-faq -->

<?php endif; ?>