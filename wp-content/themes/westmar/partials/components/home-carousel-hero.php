<?php
/**
 * Hero Carousel Section.
 *
 * Populates the hero carousel seciton.
 *
 * @package WordPress
 * @since 1.0
 */
$hero_carousel 		= get_post_meta( $post->ID, 'hero_carousel', true );
$show_navigation 	= ( count( $hero_carousel ) > 1 ) ? true : false;

if ( ! empty( $hero_carousel ) ) : ?>

<section class="hero hero--carousel">

	<?php if ( $show_navigation ) : ?>
	<div class="hero__controls hero__controls--previous">
		<span class="previous"><svg width="36px" height="19px" viewBox="0 0 36 18.5"><polygon points="18,18.7 0,0.7 0.7,0 18,17.2 35.2,0 35.9,0.7   "></polygon></svg></span>
	</div>
	<?php endif; ?>

	<?php
	$i = 0;
	foreach ( $hero_carousel as $hero_carousel ) :

		if ( ! $hero_carousel = 'carousel_item' ) :
			return;
		endif;

		$type 				= get_post_meta( $post->ID, 'hero_carousel_' . $i . '_item_type', true );
		$logo 				= get_post_meta( $post->ID, 'hero_carousel_' . $i . '_logo', true );
		$text 				= get_post_meta( $post->ID, 'hero_carousel_' . $i . '_text', true );
		$image 				= get_post_meta( $post->ID, 'hero_carousel_' . $i . '_image', true );
		$bottom_bar 		= get_post_meta( $post->ID, 'hero_carousel_' . $i . '_bottom_bar', true );
		$image_url 			= wp_get_attachment_image_url( $image, 'full' );
		$slide_background 	= ( 'photo' == $type ) ? 'style="background-image: url(' . $image_url . ')"' : null;

		// Video files:
		$video_file_mp4 	= wp_get_attachment_url( get_post_meta( $post->ID, 'hero_carousel_' . $i . '_video_file_mp4', true ) );
		$video_file_webm 	= wp_get_attachment_url( get_post_meta( $post->ID, 'hero_carousel_' . $i . '_video_file_webm', true ) );
		$video_file_ogv 	= wp_get_attachment_url( get_post_meta( $post->ID, 'hero_carousel_' . $i . '_video_file_ogv', true ) );
		$video_poster 		= wp_get_attachment_url( get_post_meta( $post->ID, 'hero_carousel_' . $i . '_video_poster', true ) );
		?>

		<div class="slide" <?php echo $slide_background; ?>>

			<?php if ( 'video' == $type ) : ?>

				<div class="slide__video">
					<video autoplay muted loop poster="<?php echo $video_poster; ?>">
						<source src="<?php echo $video_file_webm; ?>" type='video/webm;codecs="vp8, vorbis"'/>
						<source src="<?php echo $video_file_mp4; ?>" type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
						<source src="<?php echo $video_file_ogv; ?>" type="video/ogv"/>
					</video>
				</div>

			<?php endif; ?>

			<div class="slide__overlay" <?php echo $overlay_style; ?>></div>

			<div class="container">

				<div class="slide__caption">

					<div class="slide__logo">
						<?php echo wp_get_attachment_image( $logo, 'large', false, array() ); ?>
					</div>

					<ul class="slide__bedrooms">
						<li><?php echo __( $text, 'wm' ); ?></li>
                        <li><a href="/floor-plans/#1-bedrooms">1 Bed</a></li>
						<li><a href="/floor-plans/#2-bedrooms">2 Bed</a></li>
						<li><a href="/floor-plans/#3-bedrooms">3 Bed</a></li>
						<li><a href="/floor-plans/#4-bedrooms">4 Bed</a></li>
					</ul>

			</div><!-- .slide__caption -->

		</div><!-- .container -->

		<?php if ( ! empty( $bottom_bar ) ) : ?>

			<div class="slide__bar">
				<div class="container">
					<?php echo $bottom_bar; ?>
				</div>
			</div>

		<?php endif; ?>

	</div><!-- .slide -->

	<?php// endfor; ?>

<?php $i++; ?>
<?php endforeach; ?>

	<?php if ( $show_navigation ) : ?>
	<div class="hero__controls hero__controls--next">
		<span class="next"><svg  width="36px" height="19px" viewBox="0 0 36 18.5"><polygon points="18,18.7 0,0.7 0.7,0 18,17.2 35.2,0 35.9,0.7   "></polygon></svg> </span>
	</div>
	<?php endif; ?>

<!-- 	<div class="scroll">
		<a href="#page-content">
			<svg viewBox="0 0 36 18.5"><polygon points="18,18.7 0,0.7 0.7,0 18,17.2 35.2,0 35.9,0.7   "></polygon></svg>
		</a>
	</div> -->

</section>
<?php endif; ?>
