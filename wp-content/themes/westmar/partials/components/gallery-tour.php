<?php
$gallery_tour_title 	= get_post_meta( $post->ID, 'gallery_tour_title', true );
$gallery_tour_video 	= get_post_meta( $post->ID, 'gallery_tour_video_code', true );
?>

<div class="gallery-tour">
	<div class="container">
		<?php if ( ! empty( $gallery_tour_title ) && ! empty( $gallery_tour_video ) ) : ?>

			<h3 class="gallery-tour__title"><?php echo __( $gallery_tour_title, 'wm' ); ?></h3>

			<?php
			if ( ! empty( $gallery_tour_video ) ) :
				echo $gallery_tour_video;
			endif;
			?>

		<?php endif; ?>
	</div>
</div>