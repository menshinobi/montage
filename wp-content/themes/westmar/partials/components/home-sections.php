<?php
$sections = get_post_meta( $post->ID, 'sections', true );

// wp_die( '<pre>' . var_export( $sections, true ) . '</pre>' );

$i = 0;
foreach ( $sections as $section ) :

	/**
	 * Hero section.
	 */
	if ( 'hero_section' == $section ) :

		$hero_text 				= get_post_meta( $post->ID, 'sections_' . $i . '_hero_text', true );
		$hero_text_placement 	= get_post_meta( $post->ID, 'sections_' . $i . '_hero_text_placement', true );
		$hero_text_color 		= get_post_meta( $post->ID, 'sections_' . $i . '_hero_text_color', true );
		$hero_image_id 			= get_post_meta( $post->ID, 'sections_' . $i . '_hero_image', true );

		$hero_class = 'section-hero--' . $hero_text_placement . ' section-hero--' . $hero_text_color;
		$hero_style = 'style="background-image: url(' . wp_get_attachment_url( $hero_image_id, "full") . ')"';
		?>

		<div class="section-hero <?php echo $hero_class; ?>">
			<div class="section-hero__background" <?php echo $hero_style; ?>></div>
			<div class="container">
				<div class="hero__text">
					<span><?php echo __( $hero_text, 'wm' ); ?></span>
					<div class="dot-line"></div>
				</div>
			</div>
		</div>

	<?php endif;




	/**
	 * Feature section.
	 */
	if ( 'feature_section' == $section ) :

		$feature_title 			= get_post_meta( $post->ID, 'sections_' . $i . '_feature_title', true );
		$feature_pattern 		= get_post_meta( $post->ID, 'sections_' . $i . '_feature_pattern', true );
		$feature_image 			= get_post_meta( $post->ID, 'sections_' . $i . '_feature_image', true );
		$feature_image_tall 	= get_post_meta( $post->ID, 'sections_' . $i . '_feature_image_tall', true );
		$feature_image_small 	= get_post_meta( $post->ID, 'sections_' . $i . '_feature_image_small', true );

		$feature_image_caption  		= get_post($feature_image)->post_excerpt;
		$feature_image_tall_caption  	= get_post($feature_image_tall)->post_excerpt;
		$feature_image_small_caption  	= get_post($feature_image_small)->post_excerpt;

		$class = ' has-pattern-' . $feature_pattern;
		?>

		<div class="wm-feature<?php echo $class; ?>">
			<div class="container">

				<div class="wm-feature__image-main">
					<div class="inner">
						<div class="overlay"><span><?php echo $feature_image_caption; ?></span></div>
						<?php echo wp_get_attachment_image( $feature_image, 'large', false, array() ); ?>
					</div>
				</div>

				<div class="wm-feature__content">

					<h3 class="wm-feature__title"><?php echo __( $feature_title, 'wm' ); ?></h3>

					<div class="wm-feature__image wm-feature__image--tall">
						<div class="inner">
							<div class="overlay"><span><?php echo $feature_image_tall_caption; ?></span></div>
							<?php echo wp_get_attachment_image( $feature_image_tall, 'large', false, array() ); ?>
						</div>
					</div>

					<div class="wm-feature__image">
						<div class="inner">
							<div class="overlay"><span><?php echo $feature_image_small_caption; ?></span></div>
							<?php echo wp_get_attachment_image( $feature_image_small, 'large', false, array() ); ?>
						</div>
					</div>

				</div>

			</div>
		</div>

	<?php endif;




	/**
	 * Icon Grid
	 */
	if ( 'icon_grid' == $section ) :

		$icon_grid_items 	= get_post_meta( $post->ID, 'sections_' . $i . '_icon_grid_items', true );

		if ( $icon_grid_items ) : ?>

			<div class="wm-icon-grid">

				<div class="container">

					<?php for( $x = 0; $x < $icon_grid_items; $x++ ) :
						$icon 			= get_post_meta( $post->ID, 'sections_' . $i . '_icon_grid_items_' . $x . '_icon', true );
						$title 			= get_post_meta( $post->ID, 'sections_' . $i . '_icon_grid_items_' . $x . '_title', true );
						$description 	= get_post_meta( $post->ID, 'sections_' . $i . '_icon_grid_items_' . $x . '_description', true );
					?>

						<div class="item">
							<div class="item__icon"><?php echo wp_get_attachment_image( $icon, 'medium', false, array() ); ?></div>
							<h4 class="item__title"><?php echo $title; ?></h4>
							<p class="item__description"><?php echo $description; ?></p>
						</div>

					<?php endfor; ?>

				</div>

			</div><!-- .wm-icon-grid -->

		<?php endif; ?>

	<?php endif;





	/**
	 * Available Units
	 */
	if ( 'units' == $section ) :

		$available_units = get_post_meta( $post->ID, 'sections_' . $i . '_available_units', true );

		if ( $available_units ) : ?>

			<div class="wm-units">

				<div class="container">

					<?php for( $x = 0; $x < 4; $x++ ) :
						$bedrooms 		= get_post_meta( $post->ID, 'sections_' . $i . '_available_units_' . $x . '_bedrooms', true );
						$square_feet 	= get_post_meta( $post->ID, 'sections_' . $i . '_available_units_' . $x . '_square_feet', true );
						$button_text 	= get_post_meta( $post->ID, 'sections_' . $i . '_available_units_' . $x . '_button_text', true );
						$button_link 	= get_post_meta( $post->ID, 'sections_' . $i . '_available_units_' . $x . '_button_link', true );
					?>

						<div class="unit">
							<h4 class="unit__title"><?php echo $x + 1; ?> <?php echo ($x == 0) ? 'bedroom' : 'bedrooms'; ?></h4>
							<div class="unit__size">
								<img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
								<?php if ('two' == $bedrooms): ?>
                                <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
                                <?php endif; ?>
								<?php if ( 'three' == $bedrooms ) : ?>
									<img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
                                    <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
								<?php endif; ?>
								<?php if ( 'four' == $bedrooms ) : ?>
									<img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
									<img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
                                    <img src="<?php echo THEME_URL; ?>/assets/img/icon-roomate.png" alt="roomate-icon">
								<?php endif; ?>
								<span><?php echo number_format( $square_feet ); ?> sq. ft.</span>
							</div>
							<a href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
						</div>

					<?php endfor; ?>

				</div>

			</div><!-- .wm-icon-grid -->

		<?php endif; ?>

	<?php endif;






	/**
	 * Affiliates section.
	 */
	if ( 'affiiates_section' == $section ) :

		$affiliates_title 	= get_post_meta( $post->ID, 'sections_' . $i . '_affiliates_title', true );
		$affiliates 		= get_post_meta( $post->ID, 'sections_' . $i . '_affiliates', true );

		?>

		<div class="affiliates">
			<div class="container">
				<h4 class="affiliates__title"><?php echo __( $affiliates_title, 'wm' ); ?></h4>

				<?php for( $z = 0; $z < $affiliates; $z++ ) : ?>

						<?php $logo_id 		= get_post_meta( $post->ID, 'sections_' . $i . '_affiliates_' . $z . '_affiliates_logo', true ); ?>
						<?php $link 		= get_post_meta( $post->ID, 'sections_' . $i . '_affiliates_' . $z . '_affiliate_link', true ); ?>

						<div class="affiliate">
							<?php if ( ! empty( $link ) ) : ?><a target="_blank" href="<?php echo $link; ?>"><?php endif; ?>
								<div class="affiliate__logo"><img src="<?php echo wp_get_attachment_url( $logo_id, 'large'); ?>" alt=""></div>
							<?php if ( ! empty( $link ) ) : ?></a><?php endif; ?>
						</div>

				<?php endfor; ?>

			</div>
		</div><!-- affiliates -->

	<?php endif;






$i++;
endforeach;

?>