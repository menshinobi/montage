<?php
$shuttle_link 		= get_post_meta( $post->ID, 'shuttle_link', true );
$shuttle_link_text 	= get_post_meta( $post->ID, 'shuttle_link_text', true );
$shuttle_features 	= get_post_meta( $post->ID, 'shuttle_features', true );
?>

<div class="shuttle">
	<div class="container">

		<?php if ( ! empty( $shuttle_link ) ) : ?>
		<div class="shuttle__link">
			<a href="<?php echo $shuttle_link; ?>" target="_blank"><?php echo $shuttle_link_text; ?></a>
		</div>
		<?php endif; ?>

		<ul class="shuttle__features">
			<?php
			$shuttle_features  = get_post_meta( $post->ID, 'shuttle_features', true );
			if( shuttle_features ) :

				for( $i = 0; $i < $shuttle_features; $i++ ) :
					$feature_icon_id 		= get_post_meta( $post->ID, 'shuttle_features_' . $i . '_feature_icon', true );
					$feature_title 			= get_post_meta( $post->ID, 'shuttle_features_' . $i . '_feature_title', true );
					$feature_description 	= get_post_meta( $post->ID, 'shuttle_features_' . $i . '_feature_description', true );
				?>

				<li class="feature">
					<div class="feature__icon"><?php echo wp_get_attachment_image( $feature_icon_id, 'medium', false, array() ); ?></div>
					<div class="feature__title"><?php echo $feature_title; ?></div>
					<div class="feature__description"><?php echo wpautop( $feature_description ); ?></div>
				</li>

				<?php endfor; ?>

			<?php endif; ?>

		</ul>

	</div>
</div>