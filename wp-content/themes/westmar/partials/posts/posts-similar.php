<?php
/**
 * Adds "Next" and "Previous" posts section
 *
 * This will add links to the next and previous posts,
 * when shown on a single.php file.
 *
 * @package WordPress
 * @since 1.0
 */

$prev_post = get_previous_post();
$next_post = get_next_post();

if ( !empty( $prev_post) || !empty( $next_post ) ) : ?>

	<div class="similar-posts">
		
		<h3 class="similar-posts__title">Related Posts</h3>

		<?php if ( is_a( $prev_post, 'WP_Post' ) ) : ?>
	
			<?php
				if ( has_post_thumbnail( $prev_post->ID ) ) :
					$thumbnail_url = get_the_post_thumbnail_url( $prev_post->ID );
					$style = '';
					$style = 'style="background: url(' . $thumbnail_url . ') no-repeat center center / cover"';
				else :
					$style = '';
				endif;
			?>

			<div class="similar-post similar-post--next">
				
				<div class="inner">
					<a href="<?php echo get_permalink( $prev_post->ID ); ?>">
						<div class="similar-post__image" <?php echo $style; ?>></div>
					</a>
					<a href="<?php echo get_permalink( $prev_post->ID ); ?>"><h4 class="similar-post__title"><?php echo get_the_title( $prev_post->ID ); ?></h4></a>
				</div>
			
			</div>
			<!-- .previous-post -->

		<?php endif; ?>

		<?php if ( is_a( $next_post, 'WP_Post' ) ) : ?>
			
			<?php
				if ( has_post_thumbnail( $next_post->ID ) ) :
					$thumbnail_url = get_the_post_thumbnail_url( $next_post->ID );
					$style = '';
					$style = 'style="background: url(' . $thumbnail_url . ') no-repeat center center / cover"';
				else :
					$style = '';
				endif;
			?>

			<div class="similar-post similar-post--next">
				
				<div class="inner">
					<a href="<?php echo get_permalink( $next_post->ID ); ?>">
						<div class="similar-post__image" <?php echo $style; ?>></div>
					</a>
					<a href="<?php echo get_permalink( $next_post->ID ); ?>"><h4 class="similar-post__title"><?php echo get_the_title( $next_post->ID ); ?></h4></a>
				</div>
			
			</div>

		<?php endif; ?>

	</div>
	<!-- .similar-posts -->

<?php endif;
