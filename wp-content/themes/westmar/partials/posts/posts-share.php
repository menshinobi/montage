<?php
/**
 * Sharing Buttons
 *
 * Displays sharing buttons
 *
 * @package WordPress
 * @since 1.0
 */

global $post;

$permalink = get_the_permalink();
$title = get_the_title();
$thumbnail = get_the_post_thumbnail_url( $post->ID );
?>

<ul class="social-sharing">
	<li class="facebook"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( $permalink ); ?>"><i class="fa fa-facebook"></i></a></li>
	<li class="twitter"><a target="_blank" href="https://twitter.com/home?status=<?php echo esc_url( $permalink ); ?>"><i class="fa fa-twitter"></i></a></li>
	<li class="pinterst"><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo esc_url( $permalink ); ?>&media=<?php echo $thumbnail; ?>&description=<?php echo $title; ?>"><i class="fa fa-pinterest"></i></a></li>
	<li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo esc_url( $permalink ); ?>&title=<?php echo $title; ?>&summary=&source="><i class="fa fa-linkedin"></i></a></li>
	<li class="google-plus"><a target="_blank" href="https://plus.google.com/share?url=<?php echo esc_url( $permalink ); ?>"><i class="fa fa-google-plus"></i></a></li>
</ul>
