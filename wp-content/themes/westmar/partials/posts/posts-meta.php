<?php
/**
 * Post Meta Information
 *
 * Shows additional post information.
 *
 * @package WordPress
 * @since 1.0
 */

global $post;

// Post author data.
$author_id 			= get_the_author_ID();
$author_image 		= get_user_meta( $author_id, 'author_image', true );
$author_image_url 	= wp_get_attachment_image_url( $author_image, 'thumbnail' );
?>

<div class="post-meta">

	<div class="inner">

		<div class="post-author">

			<?php if ( ! empty( $author_image_url ) ) : ?>
				<img class="post-author__image "src="<?php echo esc_url( $author_image_url ); ?>" alt="Author profile photo"/>
			<?php endif; ?>

			<div class="post-author__name"><a href="<?php echo get_author_posts_url( $author_id );?>"><?php echo get_the_author(); ?></a></div>
		</div>
		<!-- post-author -->

		<div class="post-date"><?php echo get_the_date('F j, Y'); ?></div>

		<?php $categories = get_the_category(); ?>
		<?php if ( ! empty( $categories ) ) : ?>

			<div class="post-categories">
				<?php foreach ( $categories as $cat ) : ?>
					<span><?php echo $cat->name; ?></span>
				<?php endforeach; ?>
			</div>
			<!-- .post-categories -->

		<?php endif; ?>

		<div class="post-share">
			<?php get_template_part( 'partials/posts/posts-share' ); ?>
		</div>

		<div class="post-comments">

			<?php $comments = wp_count_comments( get_the_ID() ); ?>
			<?php if ( 0 < $comments->approved ) : ?>
				<a class="scroll" href="#post-comments"><i class="fa fa-comments fa"></i> Comments: <?php echo $comments->approved; ?></a>
			<?php else : ?>
				<a class="scroll" href="#post-comments"><i class="fa fa-comments fa"></i> Comment</a>
			<?php endif; ?>

		</div>
		<!-- .post-comments -->

	</div>
	<!-- .inner -->

</div>
<!-- .post-meta -->
