<?php
/**
 *	Default file, used for
 *	blog / archive layouts.
 *
 *	@package WordPress
 *	@since 1.0
 */

get_header();
?>

<div class="page-wrap">

	<?php if ( have_posts() ) : ?>

		<?php get_template_part( 'partials/components/page-header', 'index' ); ?>

		<div class="page-content-wrap container">

			<div class="page__content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'partials/content/content', 'post' ); ?>
				<?php endwhile; ?>

				<div class="pagination">
					<?php
					global $wp_query;
					$big = 999999999;
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var( 'paged' ) ),
						'total' => $wp_query->max_num_pages,
						'prev_text'          => __('<i class="fa fa-angle-left"></i>'),
						'next_text'          => __('<i class="fa fa-angle-right"></i>'),
					) );
					?>
				</div><!-- .pagination -->

			</div><!-- .page__content -->

			<div class="sidebar">
				<?php dynamic_sidebar( 'blog-sidebar' ); ?>
			</div>

		</div><!-- END .page-content-wrap -->

<?php endif; ?>

</div><!-- END .page-wrap -->
<?php get_footer(); ?>
